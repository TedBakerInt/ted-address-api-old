[{
	"AccountName": "Aquerreta 1964 SL",
	"Number": "CGB003182",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026520",
		"Description": "Aquerreta 1964 SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB215 Customer",
	"Number": "SCAGB215",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005548",
		"Description": "215 - LIVINGSTON - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Al-Mutawa And Al-Khatib Co",
	"Number": "CGB001229",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013073",
		"Description": "AL-MUTAWA AND AL-KHATIB CO",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Maria Jose Ortega Fernandez t/a Btq Majo",
	"Number": "CGB005829",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031513",
		"Description": "Maria Jose Ortega Fernandez t/a Btq Majo",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "michelle gunn",
	"Number": "CGB004696",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Het And Heads",
	"Number": "CGB000826",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007724",
		"Description": "Het And Heads",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lucie Southall",
	"Number": "CGB002027",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB399 Customer",
	"Number": "SCAGB399",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005751",
		"Description": "NON TED HOF CONCESSION",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bloomingdales Newport Beach Fashion",
	"Number": "CGB010256",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046374",
		"Description": "Bloomingdales Los Angeles DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046608",
		"Description": "Bloomingdales Newport Beach Fashion",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Netherlands - Syncreon Asi",
	"Number": "CGB000693",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006617",
		"Description": "Apple Netherlands - Syncreon Asi",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Iysha Malik",
	"Number": "CGB009154",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lavinia Balau",
	"Number": "CGB002726",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Arabella Digby",
	"Number": "CGB001909",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Cia de la Moda SMS",
	"Number": "CGB001687",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024721",
		"Description": "Cia de la Moda SMS Delivery",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "aaron Payler",
	"Number": "CGB001571",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lucy Harper",
	"Number": "CGB008532",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Matthew Jeffrey",
	"Number": "CGB003576",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Maze Clothing [Uk] LTD",
	"Number": "CGB000172",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008059",
		"Description": "Maze Clothing [Uk] LTD",
		"email": "mazemenswear@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Journeys Friend Exports LTD [Ncl]",
	"Number": "CGB000603",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007848",
		"Description": "Journeys Friend Exports LTD",
		"email": "jfx@nclbond.force9.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB905 Customer",
	"Number": "SCAGB905",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005759",
		"Description": "JOHN LEWIS - PETERBOROUGH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Parkstone LTD T-A Hoopers Inv",
	"Number": "CGB000014",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007757",
		"Description": "Parkstone LTD T-A Hoopers Inv",
		"email": "fashadmin@hoopers.ltd.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Zalando Se Invoices",
	"Number": "CGB001094",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008483",
		"Description": "Zalando Se",
		"email": "accounting@zalando.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Lingerie Room",
	"Number": "CGB008780",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044710",
		"Description": "The Lingerie Room",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB319 Customer",
	"Number": "SCAGB319",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005617",
		"Description": "HOUSE OF FRASER READING",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bellissima [Cheshire] Ltd",
	"Number": "CGB003622",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027768",
		"Description": "Bellissima {Cheshire] Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Milton Keynes",
	"Number": "CUKCC0034",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149556",
		"Description": "HOF - Milton Keynes",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "marta martyniuk",
	"Number": "CGB006114",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ondine Baker",
	"Number": "CGB006032",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "zena abid",
	"Number": "CGB001586",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB254 Customer",
	"Number": "SCAGB254",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005585",
		"Description": "WESTFIELD 1 - TED BAKER MAINLINE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Andrew Martin International LTD",
	"Number": "CGB000024",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006876",
		"Description": "Andrew Martin International LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
	"Number": "CGB007455",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041205",
		"Description": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Attica Department Stores Sa",
	"Number": "CGB000740",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008086",
		"Description": "Attica Department Stores Sa",
		"email": "paleochoriti@atticadps.gr; chatzina@atticadps.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tara Scott",
	"Number": "CGB003237",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Psycle Ltd t/a Psycle London M. St",
	"Number": "CGB006246",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036589",
		"Description": "Psycle Ltd t/a Psycle London M.St",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Footasylum LTD",
	"Number": "CGB001279",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007356",
		"Description": "Footasylum LTD",
		"email": "david.fisher@footasylum.com; accounts@footasylum.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB265 Customer",
	"Number": "SCAGB265",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005599",
		"Description": "LIVERPOOL ONE - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tessuti LTD C-O Jd Sports Fashion",
	"Number": "CGB000877",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008571",
		"Description": "Tessuti LTD C-O Jd Sports Fashion",
		"email": "merch@tessuti.co.uk; swaite@tessuti.co.uk; gholland@tessuti.co.uk; ccuthbert@tessuti.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charm Luxury",
	"Number": "CGB000823",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007077",
		"Description": "Charm Luxury",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lorna Reaoch",
	"Number": "CGB002651",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "kelsey maria bimpson",
	"Number": "CGB003304",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Adam Taylor",
	"Number": "CGB003280",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Emma H LTD",
	"Number": "CGB000773",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007310",
		"Description": "Emma H LTD",
		"email": "christineb@emmah.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "oliver Gilbert",
	"Number": "CGB005041",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "William Major LTD [Invoices]",
	"Number": "CGB000019",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007916",
		"Description": "William Major LTD [Invoices]",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Andrew Colley CPS",
	"Number": "CGB003674",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045592",
		"Description": "Store",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "PYA Importer Ltd",
	"Number": "CGB000004",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008244",
		"Description": "P.Y.A. Importer LTD",
		"email": "nicole@pyaimporter.com: caitlin@pyaimporter.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "James Alves",
	"Number": "CGB009381",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Arron Caufield",
	"Number": "CGB001426",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Debra McGrory",
	"Number": "CGB001528",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000032036",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Poem Collection",
	"Number": "CGB000762",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008204",
		"Description": "Poem Collection",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Holeshot LTD Ta Hs2",
	"Number": "CGB000046",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007769",
		"Description": "Holeshot LTD Ta Hs2",
		"email": "holeshot@cwgsy.net",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Shiree Smith T-A Curves Lingerie",
	"Number": "CGB000635",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007155",
		"Description": "Shiree Smith T-A Curves Lingerie",
		"email": "shiree@curves-lingerie.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elys - Wimbledon",
	"Number": "CUKCC0052",
	"Group": "R-CON-ELY",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149574",
		"Description": "Elys - Wimbledon",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SEEMA DHIMAN",
	"Number": "CGB010287",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Olivia Fashion",
	"Number": "CGB000834",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008114",
		"Description": "Olivia Fashion",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sebastian",
	"Number": "CGB001311",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018826",
		"Description": "Sebastian",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Cactus",
	"Number": "CGB000078",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007167",
		"Description": "Cactus",
		"email": "shelleywisner@yahoo.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ross Cooke",
	"Number": "CGB002543",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mercedes Vicente Alegria t/a Amarelo",
	"Number": "CGB008117",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043143",
		"Description": "Mercedes Vicente Alegria t/a Amarelo",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "243 Bluewater",
	"Number": "CGB001773",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004189",
		"Description": "243 Bluewater",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "David Angel",
	"Number": "CGB001799",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Michelle Mullally",
	"Number": "CGB001910",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hannah Vowles",
	"Number": "CGB003554",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "245 Brighton",
	"Number": "CGB005627",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002993",
		"Description": "245 Brighton",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges Food Hall",
	"Number": "CGB000928",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007878",
		"Description": "Selfridges Food Hall",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Ls4 Man Strategic",
	"Number": "CGB000751",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007623",
		"Description": "Lsg Sky Chefs Uk LTD - Manchester",
		"email": "laura.razmaite@lsgskychefs.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Beth Taylor CPS",
	"Number": "CGB002983",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030898",
		"Description": "Beth Taylor CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "emma jenkins",
	"Number": "CGB003384",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Stockmann Helsinki",
	"Number": "CGB000181",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008517",
		"Description": "Stockmann Helsinki",
		"email": "jari@acc3ss.com; samu@acc3ss.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges- Manchester Exchange Sq.",
	"Number": "CUKCC0003",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149525",
		"Description": "Selfridges- Manchester Exchange Sq.",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "250 Cambridge",
	"Number": "CGB008687",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004089",
		"Description": "250 Cambridge",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kyra Clarke",
	"Number": "CGB005948",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Innes Cole",
	"Number": "CGB000089",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007812",
		"Description": "Innes Cole",
		"email": "innescole@btopenworld.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarl Sabot T-A Le Boudoir",
	"Number": "CGB000841",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007513",
		"Description": "Sarl Sabot T-A Le Boudoir",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Matthew James",
	"Number": "CGB009788",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Spence MISC do not use",
	"Number": " Sean",
	"Group": "",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "Sean Spencer CPS",
		"Description": "R-RETAILCU",
		"email": "000032503",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[Sean Spencer CPS] is greater than 15 characters)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Folli-Follie Group SMS",
	"Number": "CGB000931",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007688",
		"Description": "Folli-Follie Group SMS",
		"email": "stocco@follifollie.gr; nkarellis@follifollie.gr; zachari@follifollie.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Northern Threads Lifestyle LTD",
	"Number": "CGB000143",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008078",
		"Description": "Northern Threads Deliveries",
		"email": "northernthreads@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nancy Hall",
	"Number": "CGB001788",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Al Fardous Gulf Arabian Tradin",
	"Number": "CGB001256",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013109",
		"Description": "AL FARDOUS GULF ARABIAN TRADIN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fitzgerald Menswear Ltd",
	"Number": "CGB001804",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018749",
		"Description": "Fitzgerald Menswear Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB903 Customer",
	"Number": "SCAGB903",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005757",
		"Description": "JOHN LEWIS - SLOANE SQUARE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "anna O'doherty",
	"Number": "CGB002413",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Tjx Uk Liq",
	"Number": "CGB000025",
	"Group": "W-OFFPRICE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008603",
		"Description": "Tjx Uk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Van Der Kam Zutphen Mens",
	"Number": "CGB001160",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008667",
		"Description": "Van Der Kam Menswear",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Paul Docherty",
	"Number": "CGB001731",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031459",
		"Description": "PAUL DOCHERTY FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "High Street LTD T-A Samuel Pepys",
	"Number": "CGB000073",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007852",
		"Description": "High Street LTD T-A Samuel Pepys",
		"email": "spepys@live.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mark Blackler",
	"Number": "CGB002005",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Modehuis Peeters",
	"Number": "CGB000942",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008017",
		"Description": "Modehuis Peeters",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB921 Customer",
	"Number": "SCAGB921",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005779",
		"Description": "JOHN LEWIS - READING",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Returns",
	"Number": "CUKCC0041",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149563",
		"Description": "HOF - Returns",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aaron West",
	"Number": "CGB003561",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Cursor C-O Eurotextil LTD",
	"Number": "CGB000888",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007684",
		"Description": "Cursor C-O Eurotextil LTD",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Yasmin Graham FAF",
	"Number": "CGB005121",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033902",
		"Description": "YASMIN GRAHAM 243",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free1",
	"Number": "CGB010752",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000150315",
		"Description": "Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free1",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": false,
	"errorMessage": "(Validation Error: Account Name[Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free1] is greater than 50 characters)",
	"errorNumber": "ESB001"
},
{
	"AccountName": "Emily Farmer CPS",
	"Number": "CGB002603",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044566",
		"Description": "kitted",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Choice LTD",
	"Number": "CGB000058",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007085",
		"Description": "Choice LTD",
		"email": "holley@choicestore.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB338 Customer",
	"Number": "SCAGB338",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005621",
		"Description": "HOUSE OF FRASER NORWICH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarah Fancourt",
	"Number": "CGB003351",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Perfomans Llc",
	"Number": "CGB001051",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008192",
		"Description": "Perfomans Llc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008193",
		"Description": "Perfomans Llc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jw Stringer LTD",
	"Number": "CGB000186",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008531",
		"Description": "Jw Stringer LTD",
		"email": "stella@stringers-lytham.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ailsa mathews",
	"Number": "CGB004085",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031525",
		"Description": "Ailsa Matthews Misc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Patrick Bourke Ennis Ltd",
	"Number": "CGB003493",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027747",
		"Description": "Patrick Bourke",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB924 Customer",
	"Number": "SCAGB924",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005782",
		"Description": "JOHN LEWIS - SOLIHULL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Flash Store",
	"Number": "CGB001322",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018836",
		"Description": "Flash Store",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "230 Floral Street",
	"Number": "CGB003712",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002981",
		"Description": "230 Floral Street",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alfa Egypt Trading Sae -",
	"Number": "CGB001252",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013105",
		"Description": "ALFA EGYPT TRADING SAE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "natalie young",
	"Number": "CGB005188",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [Canterbury] Acc",
	"Number": "CGB000137",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007370",
		"Description": "Fenwick LTD [Canterbury] Acc",
		"email": "canterbury.suppliers@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007379",
		"Description": "Fenwick LTD Service Centre",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Parkstone LTD Ta Hoopers Wilmslow",
	"Number": "CGB000128",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007801",
		"Description": "Parkstone LTD Ta Hoopers Wilmslow",
		"email": "fashadmin@hoopers.ltd.uk harriet",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ess Burgum",
	"Number": "CGB000960",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007330",
		"Description": "Ess Burgum",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lyndsey Kerr",
	"Number": "CGB003630",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000032040",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "La Scala",
	"Number": "CGB001133",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007505",
		"Description": "La Scala",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mainline Menswear Ltd",
	"Number": "CGB008870",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044865",
		"Description": "Mainline Menswear Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nordstrom Fulton Market Rack (Brooklyn NY)",
	"Number": "CGB008360",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039197",
		"Description": "0699 - Marlboro DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sunrise Online Retail LTD",
	"Number": "CGB000682",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008509",
		"Description": "Sunrise Online Retail LTD",
		"email": "hollys@underu.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Natalie Robyn Turner",
	"Number": "CGB002536",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB308 Customer",
	"Number": "SCAGB308",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005607",
		"Description": "SELFRIDGES ONLINE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "DJS Retail Ltd t/a Smiths",
	"Number": "CGB000057",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007914",
		"Description": "DJS Retail Ltd t/a Smiths",
		"email": "danny.holden@david-jason.co.uk; julie.cooper@david-jason.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Baco SRL",
	"Number": "CGB008303",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043689",
		"Description": "Trascon SRL - Baco Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Old Millhillians Club",
	"Number": "CGB001102",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007964",
		"Description": "Old Millhillians Club",
		"email": "sk@millhill.org.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JAY WILLIAMS",
	"Number": "CGB003225",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033331",
		"Description": "JAY WILLIAMS FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laura McGale",
	"Number": "CGB002164",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000025519",
		"Description": "LAURA MCGALE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashley Wilde Group Ltd",
	"Number": "CGB005183",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034029",
		"Description": "Ashley Wilde Group Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "INACTIVE Camp Hopson And Co LTD",
	"Number": "CGB001274",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013128",
		"Description": "CAMP HOPSON AND CO LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "High Society Market Harborough LTD",
	"Number": "CGB000769",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007777",
		"Description": "High Society Market Harborough LTD",
		"email": "limehouse.mh@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashworth and Bird [Jersey] LTD",
	"Number": "CGB009076",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045597",
		"Description": "Ashworth and Bird [Jersey] LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Phillips-Van Heusen Corporatio",
	"Number": "CGB001214",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013052",
		"Description": "PHILLIPS-VAN HEUSEN CORPORATIO",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nathan Wood",
	"Number": "CGB003210",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB258 Customer",
	"Number": "SCAGB258",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005590",
		"Description": "HEATHROW TERMINAL 3 - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Manchester",
	"Number": "CUKCC0030",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149552",
		"Description": "HOF - Manchester",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "elliott nettleship",
	"Number": "CGB003558",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035053",
		"Description": "DO NOT USE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "269 Spitalfields",
	"Number": "CGB008170",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Arnotts LTD Consignment W-Acc",
	"Number": "CGB000758",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006593",
		"Description": "Arnotts LTD Consignment W-Acc",
		"email": "vlong@arnotts.ie; eobyrnes@arnotts.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jonker And Co",
	"Number": "CGB000830",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007407",
		"Description": "Jonker And Co",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Daniela Julieta Silva Catanho",
	"Number": "CGB009385",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "K+K Squared Ltd t/a Foundations",
	"Number": "CGB004550",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030269",
		"Description": "K+K Squared Ltd t/a Foundations",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "nick wellborn",
	"Number": "CGB001419",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Leanne Cooper",
	"Number": "CGB001581",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB910 Customer",
	"Number": "SCAGB910",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005763",
		"Description": "JOHN LEWIS - SHEFFIELD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Marie Nolan",
	"Number": "CGB009052",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Brian Wade t/a Direction",
	"Number": "CGB000069",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007248",
		"Description": "Brian Wade t/a Direction",
		"email": "directionmenswear@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Edward Wilkinson",
	"Number": "CGB010183",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Repertoire Marlow",
	"Number": "CGB000116",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008325",
		"Description": "Repertoire Marlow",
		"email": "beverleygough@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "249 Cheapside",
	"Number": "CGB008726",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002999",
		"Description": "249 Cheapside",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Oxford Street",
	"Number": "CUKCC0056",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149578",
		"Description": "John Lewis - Oxford Street",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Boots Co PLC - Royalty",
	"Number": "CGB001187",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013004",
		"Description": "THE BOOTS CO PLC - ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LINA VAICAITIENE",
	"Number": "CGB003334",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "ailsa mannion",
	"Number": "CGB003413",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wildfire Entertainment LTD",
	"Number": "CGB001122",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008421",
		"Description": "Wildfire Entertainment LTD",
		"email": "marion@zumarestaurant.com; nina@zumarestaurant.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008423",
		"Description": "Wildfire Entertainment LTD",
		"email": "marion@zumarestaurant.com; nina@zumarestaurant.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Murray Todd",
	"Number": "CGB000053",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008051",
		"Description": "Murray Todd",
		"email": "murraytodd1@yahoo.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB349 Customer",
	"Number": "SCAGB349",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005642",
		"Description": "HOUSE OF FRASER CROYDON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "AS de Marin SL t/a Angela Serrano",
	"Number": "CGB005830",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034047",
		"Description": "AS de Marin SL t/a Angela Serrano",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Blossom Lingerie LTD",
	"Number": "CGB000938",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006761",
		"Description": "Blossom Lingerie LTD",
		"email": "alison@blossom-lingerie.co.uk; beckie@blossom-lingerie.co.uk; daniel@blossom-lingerie.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Lauren Parker",
	"Number": "CGB001162",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007625",
		"Description": "Alpha Lsg LTD - Lauren Parker",
		"email": "www.alphalsg.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JAMES HOLLYWOOD",
	"Number": "CGB003193",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jennifer Gomes",
	"Number": "CGB009033",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Grace and Favour",
	"Number": "CGB001315",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018849",
		"Description": "Grace and Favour",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB615 Customer",
	"Number": "SCAGB615",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045980",
		"Description": "STGB615 - TEDCOMM - GERMANY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "M & N SL t/a Boutique Chic",
	"Number": "CGB006081",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036154",
		"Description": "M & N SL t/a Boutique Chic",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Thalians",
	"Number": "CGB001040",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008575",
		"Description": "Thalians",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mcatamney LTD",
	"Number": "CGB000266",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007934",
		"Description": "Mcatamney LTD",
		"email": "amandaturner.ods@gmail.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007936",
		"Description": "Mcatamney LTD",
		"email": "amandaturner.ods@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB908 Customer",
	"Number": "SCAGB908",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005771",
		"Description": "JOHN LEWIS - SOUTHAMPTON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "McCalls of Lisburn Ltd",
	"Number": "CGB000148",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007944",
		"Description": "Mccalls Deliveries",
		"email": "info@mccallsoflisburn.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JULIA BELOUSOVA CPS",
	"Number": "CGB003491",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042478",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Gere Ltd t/a Coneys of Redditch",
	"Number": "CGB008786",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044887",
		"Description": "Gere Ltd t/a Coneys",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB913 Customer",
	"Number": "SCAGB913",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005767",
		"Description": "JOHN LEWIS - MILTON KEYNES",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Et Harvey LTD Ta Harveys Of Halifax",
	"Number": "CGB000289",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007342",
		"Description": "Et Harvey LTD Ta Harveys Of Halifax",
		"email": "office@harveysofhalifax.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ds - Aj Monk T-A White house",
	"Number": "CGB000101",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008435",
		"Description": "Ds - Aj Monk T-A Whitehouse",
		"email": "anne.monk57@ntlworld.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Goodridge Ltd",
	"Number": "CGB002632",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000025290",
		"Description": "John Goodridge",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "lucy dick",
	"Number": "CGB004387",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hoogenboom Women",
	"Number": "CGB004971",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033437",
		"Description": "Hoogenboom Women",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "TGR RETAIL LTD - Teds Grooming room.com",
	"Number": "CGB001224",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013063",
		"Description": "TGR RETAIL LTD - TEDS GROOMIN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - Newcastle",
	"Number": "CUKCC0045",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149567",
		"Description": "Fenwicks - Newcastle",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Miguel Costa",
	"Number": "CGB008510",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "CHANDNI NAKUM",
	"Number": "CGB003286",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nik Antonio",
	"Number": "CGB001735",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "M And M Direct LTD Statements",
	"Number": "CGB000161",
	"Group": "W-OFFPRICE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008004",
		"Description": "M And M Direct LTD Bulk Delivery",
		"email": "goodsin@mandmdirect.com; jwilliams@mandmdirect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "megan risk",
	"Number": "CGB003377",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Charlie Tosh",
	"Number": "CGB001970",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Javelin Retail LTD T-A Javelin",
	"Number": "CGB000042",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007826",
		"Description": "Javelin Retail LTD T-A Javelin",
		"email": "jclayton@javelinonline.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007840",
		"Description": "Javelin Deliveries",
		"email": "jeremy@javelinonline.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nordstrom Tysons Corner",
	"Number": "CGB007755",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039215",
		"Description": "0699 - Marlboro DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALEX JACKSON",
	"Number": "CGB009298",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Future Shorts LTD",
	"Number": "CGB001065",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006926",
		"Description": "Susan Kulkarni - Head Of Costume",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Stockmann Russia",
	"Number": "CGB000182",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008525",
		"Description": "Stockmann Russia",
		"email": "jari@acc3ss.com; samu@acc3ss.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sosan Hakimi",
	"Number": "CGB003731",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Robert Goddard LTD",
	"Number": "CGB000063",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024849",
		"Description": "RGDC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Osbourne",
	"Number": "CGB006035",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB381 Customer",
	"Number": "SCAGB381",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005649",
		"Description": "ELYS OF WIMBLEDON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lee Grant Dalton",
	"Number": "CGB001364",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Simon And Marx",
	"Number": "CGB000997",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008501",
		"Description": "Simon And Marx",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gimbrere",
	"Number": "CGB000964",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006957",
		"Description": "Gimbrere",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "James Walker",
	"Number": "CGB008973",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "IZZY ROBERTS",
	"Number": "CGB008340",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "katie harvey",
	"Number": "CGB003316",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Drew Taylor",
	"Number": "CGB002030",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bentalls PLC Kingston",
	"Number": "CGB000005",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006729",
		"Description": "Bentalls Plc Kingston",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - Tunbridge Wells",
	"Number": "CUKCC0049",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149571",
		"Description": "Fenwicks - Tunbridge Wells",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J E Beale PLC 09 Kendal",
	"Number": "CGB000133",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006741",
		"Description": "J E Beale PLC 09 Kendal",
		"email": "accounts@beales.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Keeley Morton",
	"Number": "CGB009802",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "K J Beckett Ltd",
	"Number": "CGB000085",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007477",
		"Description": "K J Beckett Ltd",
		"email": "office@kjbeckett.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB323 Customer",
	"Number": "SCAGB323",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005627",
		"Description": "HOUSE OF FRASER CARLISLE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Milton Keynes",
	"Number": "CUKCC0067",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149589",
		"Description": "John Lewis - Milton Keynes",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LUKA DIJAN",
	"Number": "CGB010261",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Pearsons [Enfield] LTD",
	"Number": "CGB000733",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007302",
		"Description": "Pearsons [Enfield] LTD",
		"email": "lindagumbrell@morleystores.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Styled By Juuls",
	"Number": "CGB000837",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007858",
		"Description": "Styled By Juuls",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Broeklynn",
	"Number": "CGB001325",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018864",
		"Description": "Broeklynn",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Guildford",
	"Number": "CUKCC0016",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149538",
		"Description": "HOF - Guildford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Cambridge",
	"Number": "CUKCC0074",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149596",
		"Description": "John Lewis - Cambridge",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Crocus Group T-A Crocus",
	"Number": "CGB001055",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007151",
		"Description": "Tc Crocus City Mall",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hot Pie LTD T-A Tempo Clothing",
	"Number": "CGB000700",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007765",
		"Description": "Hot Pie LTD T-A Tempo Clothing",
		"email": "paul@tempoclothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kate Friggieri CPS",
	"Number": "CGB003380",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030616",
		"Description": "Kate Friggeri CPS",
		"email": "caitlin.friggieri@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sam LLoyd",
	"Number": "CGB010257",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Victoria Croker - PU",
	"Number": "CGB001617",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030592",
		"Description": "Vic",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Armclaim LTD T/A Loofes Of Bury",
	"Number": "CGB000047",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007603",
		"Description": "Armclaim LTD T/A Loofes Of Bury",
		"email": "armclain@btopenworld.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Manufacture Roger Dubuis",
	"Number": "CGB000996",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007242",
		"Description": "Roger Dubuis Geneva",
		"email": "diane.barras@rogerdubuis.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007244",
		"Description": "Roger Dubuis North-East Asia",
		"email": "ungmin-c.lee@rogerdubuis.hk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000036730",
		"Description": "Roger Dubuis Taiwan",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000036731",
		"Description": "Roger Dubuis North America",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dean Sloman",
	"Number": "CGB001791",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB313 Customer",
	"Number": "SCAGB313",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005610",
		"Description": "HOUSE OF FRASER BOURNEMOUTH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Meninvest SAS t/a Menlook",
	"Number": "CGB000765",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007946",
		"Description": "Menlook  Chez Viapost",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB932 Customer",
	"Number": "SCAGB932",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005791",
		"Description": "JOHN LEWIS - YORK",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Footasylum LTD T-A Drome",
	"Number": "CGB001278",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007358",
		"Description": "Footasylum LTD T-A Drome",
		"email": "leon.mullin@footasylum.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Belfast",
	"Number": "CGB000606",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006820",
		"Description": "Alpha Flight Uk LTD - Tcx Bfs",
		"email": "bfsmaterials_control",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB370 Customer",
	"Number": "SCAGB370",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005645",
		"Description": "FENWICK NEWCASTLE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Danny Bradley",
	"Number": "CGB003367",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Neil Lowson FAF",
	"Number": "CGB001798",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033103",
		"Description": "Neil Lowson FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Belladona Ltd",
	"Number": "CGB003535",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029175",
		"Description": "Belladona",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sydney Dolling",
	"Number": "CGB002117",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Non Ted Concession",
	"Number": "CUKCC0092",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149614",
		"Description": "John Lewis - Non Ted Concession",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Anh Nguyen MISC",
	"Number": "CGB002856",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Claudia Fanni",
	"Number": "CGB001585",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Motion Menswear Ltd",
	"Number": "CGB003492",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027755",
		"Description": "Motion Menswear",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Leamington Spa",
	"Number": "CUKCC0013",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149535",
		"Description": "HOF - Leamington Spa",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Bournemouth",
	"Number": "CUKCC0009",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149531",
		"Description": "HOF - Bournemouth",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MISBEHAVIOUR - Distribution",
	"Number": "CGB000772",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036310",
		"Description": "Bleckmann Nederland BV-MISBEHAVIOUR",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Adam Blackler",
	"Number": "CGB001787",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Voisins Department Store LTD",
	"Number": "CGB000265",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008689",
		"Description": "Voisins Department Store LTD",
		"email": "voisins@voisins.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jordan Pass",
	"Number": "CGB002596",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fashion Fair - Tc Vastorg",
	"Number": "CGB001166",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007362",
		"Description": "Fashion Fair - Tc Vastorg",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Canary Wharf",
	"Number": "CUKCC0081",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149603",
		"Description": "John Lewis - Canary Wharf",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB914 Customer",
	"Number": "SCAGB914",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005768",
		"Description": "JOHN LEWIS - NOTTINGHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB312 Customer",
	"Number": "SCAGB312",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005609",
		"Description": "HOUSE OF FRASER BIRMINGHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Glasgow",
	"Number": "CUKCC0027",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149549",
		"Description": "HOF - Glasgow",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fresh Boutique",
	"Number": "CGB001319",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018834",
		"Description": "Fresh Boutique",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB243 Customer",
	"Number": "SCAGB243",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005571",
		"Description": "BLUEWATER - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Samiha MISC Sulthana",
	"Number": "CGB010534",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Guru [Grimsby] LTD",
	"Number": "CGB000070",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007696",
		"Description": "Guru [Grimsby] LTD",
		"email": "guruclothing@live.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Bracknell",
	"Number": "CUKCC0038",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149560",
		"Description": "HOF - Bracknell",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "De Bijenkorf Head Office",
	"Number": "CGB001044",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007191",
		"Description": "De Bijenkorf Distrib Centre",
		"email": "deliveries@debijenkorf.nl",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SMBS - Maison Bleue",
	"Number": "CGB001043",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007048",
		"Description": "SMBS - Maison Bleue",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "VSTCA007 Customer",
	"Number": "CGB007873",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042450",
		"Description": "STCA007 - TED BAKER - OTTOWA RIDEAU",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ARIANNE PLUMLEY",
	"Number": "CGB003618",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarl Danygirl T-A Danygirl",
	"Number": "CGB001038",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007181",
		"Description": "Sarl Danygirl T-A Danygirl",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Brandalley UK Ltd Samples",
	"Number": "CGB000160",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007038",
		"Description": "Brandalley T-A Brandalley Uk LTD",
		"email": "ukaccounts@brandalley.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dimitri Bishop",
	"Number": "CGB001527",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "nathan langley",
	"Number": "CGB001412",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "England & Wales Cricket Board Ltd",
	"Number": "CGB006281",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036684",
		"Description": "England & Wales Cricket Board",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Mcelhinney LTD",
	"Number": "CGB000081",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007403",
		"Description": "John Mcelhinney LTD",
		"email": "noel@mcelhinneys.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elys [Wimbledon] PLC-Wholesale",
	"Number": "CGB000036",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007306",
		"Description": "Elys [Wimbledon] PLC-Wholesale",
		"email": "stevenicoll@morleystores.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB324 Customer",
	"Number": "SCAGB324",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005628",
		"Description": "HOUSE OF FRASER BRISTOL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Maria Arshad",
	"Number": "CGB009251",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Maria Acevedo Pedrayes t/a Carmela",
	"Number": "CGB005836",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034283",
		"Description": "Maria Acevedo Pedrayes t/a Carmela",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Konen Bekleidungshaus KG",
	"Number": "CGB001308",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018736",
		"Description": "Konen Bekleidungshaus KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000018738",
		"Description": "Konen Bekleidungshaus KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Emily Amesbury",
	"Number": "CGB003041",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "vida2",
	"Number": "CGB010727",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149986",
		"Description": "vida2",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Reese M",
	"Number": "CGB001531",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB384 Customer",
	"Number": "SCAGB384",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005651",
		"Description": "HARRODS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ella Lamprell",
	"Number": "CGB002001",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ma Petite Robe Noire",
	"Number": "CGB000759",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008216",
		"Description": "Ma Petite Robe Noire",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rachael Roberts",
	"Number": "CGB007728",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Vente-Privee.com SMS",
	"Number": "CGB007968",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042697",
		"Description": "Vente-Privee.com SMS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - City",
	"Number": "CUKCC0024",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149546",
		"Description": "HOF - City",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB009643",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000064295",
		"Description": "Peek & Cloppenburg Vienna 420",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Parkstone Limited T-A Hoopers Tw",
	"Number": "CGB000165",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007740",
		"Description": "Parkstone Limited T-A Hoopers Tw",
		"email": "lauralaw@hoopersstores.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wp Brown LTD T-A Browns Gains",
	"Number": "CGB000754",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008471",
		"Description": "Wp Brown LTD T-A Browns York",
		"email": "info@browns-york.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alison Sykes",
	"Number": "CGB009195",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Kamila Duda",
	"Number": "CGB001631",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wiggle Limited T-A Wiggle",
	"Number": "CGB001050",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008443",
		"Description": "Wiggle Limited T-A Wiggle",
		"email": "accounts.payable@wiggle.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Proporta LTD INACTIVE",
	"Number": "CGB000848",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008222",
		"Description": "Proporta LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Van Der Kam Amersfoort",
	"Number": "CGB001155",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008671",
		"Description": "Van Der Kam Mens And Womens",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Milagros Jorge Divar t/a Coco",
	"Number": "CGB008595",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044392",
		"Description": "Milagros Jorge Divar t/a Coco",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sixty Six Lingerie Ltd",
	"Number": "CGB008868",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044909",
		"Description": "Sixty Six Lingerie Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Georgia Reid",
	"Number": "CGB004689",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035154",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Daniel Aslan",
	"Number": "CGB004573",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB223 Customer",
	"Number": "SCAGB223",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005557",
		"Description": "PORTSMOUTH - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carrington",
	"Number": "CGB001326",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018867",
		"Description": "Carrington",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Snaubs Ladies Fashion",
	"Number": "CGB001803",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018739",
		"Description": "Snaubs",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mono",
	"Number": "CGB000953",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008023",
		"Description": "Mono",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sintija Palode",
	"Number": "CGB010396",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB302 Customer",
	"Number": "SCAGB302",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005604",
		"Description": "SELFRIDGES – TRAFFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Francis Javier Marin Garcia t/a Btq Carlota",
	"Number": "CGB008996",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045361",
		"Description": "Javier Marin Garcia t/a Btq Carlota",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mr Wayne Lauder",
	"Number": "CGB001032",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008439",
		"Description": "Mr Wayne Lauder",
		"email": "willo6@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Summer Monteys- Fullam",
	"Number": "CGB005360",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mehra Kharaghani",
	"Number": "CGB003659",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042455",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB929 Customer",
	"Number": "SCAGB929",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005788",
		"Description": "JOHN LEWIS - SOUTHSEA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "House of Fraser Samples",
	"Number": "CGB005082",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033760",
		"Description": "House of Fraser Samples",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jenna Morrell .",
	"Number": "CGB002468",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Genelli Hamilton",
	"Number": "CGB004771",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Viste Trendy SL",
	"Number": "CGB003297",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026532",
		"Description": "Viste Trendy SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ted Baker Downunder Pty LTD",
	"Number": "CGB000020",
	"Group": "W-JV",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008565",
		"Description": "Ted Baker Downunder Pty LTD",
		"email": "laki.fernando@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kelly Middleton",
	"Number": "CGB001729",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mak Co.Sal",
	"Number": "CGB001246",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013094",
		"Description": "MAK CO.SAL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bentalls PLC Bracknell",
	"Number": "CGB000018",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006731",
		"Description": "Bentalls Bracknell Receiving Bay",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bloomingdales Tysons Corner Flint",
	"Number": "CGB010036",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046362",
		"Description": "Bloomingdales Joppa DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046597",
		"Description": "Bloomingdales Tysons Corner Flint",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aneeta Ramburuth",
	"Number": "CGB003236",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Chloe Mitchell",
	"Number": "CGB009950",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bill Richardson LTD T-A Kings",
	"Number": "CGB000068",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007473",
		"Description": "Bill Richardson LTD T-A Kings",
		"email": "kingsclothing@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Pretty Woman Bras [Ltd]",
	"Number": "CGB009401",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046776",
		"Description": "Pretty Woman Bras [Ltd]",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Little Soho",
	"Number": "CGB000867",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007643",
		"Description": "Little Soho",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "olga lavery",
	"Number": "CGB009796",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Studio 10",
	"Number": "CGB000176",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008359",
		"Description": "Linc - Studio 10 Deliveries",
		"email": "kerensa@studio10.be; thierry@studio10.be",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Coloplast LTD",
	"Number": "CGB001140",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007129",
		"Description": "Coloplast LTD",
		"email": "gblmh@coloplast.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Flore 39 Agencies Ltd - Lyn UBB",
	"Number": "CGB000267",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007655",
		"Description": "Flore 39 Agencies Ltd - Lyn UBB",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Asia Pacific Bulk",
	"Number": "CGB000687",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006599",
		"Description": "Apple Asia Pacific Bulk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Glasgow",
	"Number": "CUKCC0070",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149592",
		"Description": "John Lewis - Glasgow",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Repertoire",
	"Number": "CGB000010",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008299",
		"Description": "Repertoire-Ted Invoices",
		"email": "shirley@repertoirefashion.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB250 Customer",
	"Number": "SCAGB250",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005581",
		"Description": "CAMBRIDGE - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "EDYTA BRACHA",
	"Number": "CGB001814",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ws Pettit And Co LTD [Ret]",
	"Number": "CGB000109",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008240",
		"Description": "Ws Pettit And Co LTD [Ret]",
		"email": "paul@pettits.com; donna@pettits.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB261 Customer",
	"Number": "SCAGB261",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005594",
		"Description": "GATWICK N ACCESSORIES – TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Asos.Com T-A Asos.Com LTD",
	"Number": "CGB000035",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006653",
		"Description": "Asos.Com Deliveries",
		"email": "asoscollection@wtprima.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Kingston",
	"Number": "CUKCC0063",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149585",
		"Description": "John Lewis - Kingston",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "William Major LTD T-A Parker Taylor",
	"Number": "CGB000092",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007986",
		"Description": "William Major LTD T-A Parker Taylor",
		"email": "derek.mccormack57@yahoo.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laura Francis",
	"Number": "CGB001795",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Next Retail Ltd t/a Label",
	"Number": "CGB007338",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042694",
		"Description": "Next Retail Ltd t/a Label FLATPACK",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000042695",
		"Description": "Next Retail Ltd t/a Label HANGING",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Natasha Hiles CPS",
	"Number": "CGB003226",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030852",
		"Description": "Natasha Romanek",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abi Folarin DO NOT USE",
	"Number": "CGB008725",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "JF MODE BV",
	"Number": "CGB001314",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018847",
		"Description": "JF MODE BV",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "H And O LTD T-A Muse Shoes",
	"Number": "CGB000698",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007732",
		"Description": "H And O LTD T-A Muse Shoes",
		"email": "info@muse-shoes.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Stacey Norman",
	"Number": "CGB003604",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Thomson After Travel - Cs Dept",
	"Number": "CGB000324",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007651",
		"Description": "Thomson After Travel - Cs Dept",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carla Rowe",
	"Number": "CGB003551",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lauren Whittaker",
	"Number": "CGB001347",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nilson 2008 SA t/a Aston Moda",
	"Number": "CGB000729",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006663",
		"Description": "Aston Moda San Sebastian Deliveries",
		"email": "nietolecuona@gmail.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000045734",
		"Description": "Aston Moda Bilbao Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Attica Department Stores SA",
	"Number": "CGB003308",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000028379",
		"Description": "Attica Department Stores SA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Model Fen SL t/a Diana Grapin",
	"Number": "CGB008783",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044872",
		"Description": "Model Fen SL t/a Diana Grapin",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jaspal Samra",
	"Number": "CGB003658",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Zalando SE - SMS",
	"Number": "CGB001097",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008487",
		"Description": "Zalando Se - Department Samples",
		"email": "samples@zalando.de; keystyles@zalando.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harriet Milne",
	"Number": "CGB002708",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Leeds",
	"Number": "CGB008542",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018880",
		"Description": "Leeds",
		"email": "206.leeds@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarah Steadman",
	"Number": "CGB005071",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "darcey shaw bourne",
	"Number": "CGB002674",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ollyhurst Ltd t/a Union",
	"Number": "CGB003485",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027760",
		"Description": "Ollyhurst",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sam Wilkinson",
	"Number": "CGB009791",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB232 Customer",
	"Number": "SCAGB232",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005562",
		"Description": "BICESTER - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rushforths Of Ulverston",
	"Number": "CGB000628",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008349",
		"Description": "Rushforths Of Ulverston",
		"email": "lynnrushforth@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harley-Lily Jackson",
	"Number": "CGB001358",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043428",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lapel Tamworth LTD",
	"Number": "CGB000111",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007611",
		"Description": "Lapel Tamworth LTD",
		"email": "07973 796175 richard/rlf",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Zalando SE - SMS GBP",
	"Number": "CGB006919",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037572",
		"Description": "Zalando SE - SMS GBP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB001106",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006575",
		"Description": "Ansons - Ulm Dc",
		"email": "#log-import@ansons.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "jonny Mellor",
	"Number": "CGB002749",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026685",
		"Description": "Jonny Mellor- CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gemma Penge",
	"Number": "CGB002366",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg Retail Buying",
	"Number": "CGB001303",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018779",
		"Description": "Fashion ID",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB301 Customer",
	"Number": "SCAGB301",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005603",
		"Description": "SELFRIDGES LONDON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rachael Lamb",
	"Number": "CGB010578",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "No Ordinary Shoes Ltd",
	"Number": "CGB000154",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000052696",
		"Description": "Pentland Delivery 1 Mktg",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000052697",
		"Description": "Pentland Delivery 2 Staff Shop",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jalux Inc",
	"Number": "CGB001101",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007830",
		"Description": "Jalux Inc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB212 Customer",
	"Number": "SCAGB212",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005545",
		"Description": "GLASGOW - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jozenzo",
	"Number": "CGB001049",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007451",
		"Description": "Jozenzo",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "239 Guildford",
	"Number": "CGB005786",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005226",
		"Description": "239 Guildford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Orangebag.Nl",
	"Number": "CGB000819",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008130",
		"Description": "Orangebag.Nl",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Attica Department Stores Sa",
	"Number": "CGB000845",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007459",
		"Description": "Attica Department Stores Sa",
		"email": "gourni@atticadps.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Claire Bastian",
	"Number": "CGB001632",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Beau Brummel - Forever Changes",
	"Number": "CGB001117",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006701",
		"Description": "Beau Brummel",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Exeter",
	"Number": "CUKCC0085",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149607",
		"Description": "John Lewis - Exeter",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Debenhams PLC",
	"Number": "CGB000256",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007212",
		"Description": "Debenhams PLC Peterbro DC 0736",
		"email": "centralbookings@debenhams.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000018878",
		"Description": "Debenhams PLC Sherburn DC 0744",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Sheffield",
	"Number": "CUKCC0007",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149529",
		"Description": "HOF - Sheffield",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MILES BEER",
	"Number": "CGB003341",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB918 Customer",
	"Number": "SCAGB918",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005775",
		"Description": "JOHN LEWIS - TRAFFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fitzgerald's Menswear Cork",
	"Number": "CGB003486",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027762",
		"Description": "Fitzgerald's",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - Colchester",
	"Number": "CUKCC0051",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149378",
		"Description": "STGB - FENWICKS COLCHESTER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fran Holgado",
	"Number": "CGB001420",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "SUSAN LAGAN",
	"Number": "CGB001675",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034861",
		"Description": "DO NOT USE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rsh Singapore Pte LTD Royalty",
	"Number": "CGB001202",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013028",
		"Description": "RSH SINGAPORE PTE LTD ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Brandalley LTD",
	"Number": "CGB000159",
	"Group": "W-OFFPRICE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007036",
		"Description": "SEKO Logistics - Brandalley",
		"email": "brandalley@amethystgroup.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Next Retail Ltd t/a Label Statements",
	"Number": "CGB010017",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000052013",
		"Description": "Next Retail Ltd t/a Label Statements",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bazaar S.L",
	"Number": "CGB003204",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026523",
		"Description": "Bazaar SL t/a Boutique Ararat",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Beki North DO NOT USE",
	"Number": "CGB004631",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Serena Mitchell",
	"Number": "CGB001416",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sian Samways",
	"Number": "CGB009539",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wiggle Limited T-A Wiggle",
	"Number": "CGB001257",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013111",
		"Description": "WIGGLE LIMITED T-A WIGGLE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Haeja Sadiq",
	"Number": "CGB010499",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Roy Tennant - MISC",
	"Number": "CGB005652",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002937",
		"Description": "Roy Tennant",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Noon Fashion",
	"Number": "CGB000029",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008074",
		"Description": "Noon Fashion",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gentian Qema",
	"Number": "CGB009785",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Abbey Wilcox",
	"Number": "CGB008130",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Addict",
	"Number": "CGB000761",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006791",
		"Description": "Addict Deliveries - Saga France",
		"email": "m.sainsard@sagactl.com; v.billot@sagactl.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "lewis kincaid",
	"Number": "CGB005058",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ivor Boyd Menswear",
	"Number": "CGB000718",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007824",
		"Description": "Ivor Boyd Menswear",
		"email": "menswear@logansfashions.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
	"Number": "CGB007457",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041207",
		"Description": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Yolanda de la Fuente t/a BTQ. Backstage",
	"Number": "CGB003313",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027730",
		"Description": "BTQ. Backstage",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rico Mode Uomo",
	"Number": "CGB001151",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008309",
		"Description": "Rico Mode Uomo",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Shop Direct Home Shopping LTD SMS",
	"Number": "CGB000122",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007585",
		"Description": "Shop Direct Home Shopping LTD - SMS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007587",
		"Description": "Shop Direct Home Shopping LTD - SMS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Circle Hd Limited T-A Circle",
	"Number": "CGB000294",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007081",
		"Description": "Circle Hd Limited T-A Circle",
		"email": "abbas_27@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB342 Customer",
	"Number": "SCAGB342",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005640",
		"Description": "HOUSE OF FRASER LEEDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Louis Copeland And Sons LTD",
	"Number": "CGB001111",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007517",
		"Description": "Louis Copeland And Sons LTD",
		"email": "louisj@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Norwich",
	"Number": "CUKCC0033",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149555",
		"Description": "HOF - Norwich",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Parkstone LTD Ta Hoopers Torquay",
	"Number": "CGB000129",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007781",
		"Description": "Parkstone LTD Ta Hoopers Torquay",
		"email": "tonyarmstrong@hoopersstores.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "FDD93",
	"Number": "CGB010705",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149921",
		"Description": "ADD1",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laila Tyler",
	"Number": "CGB004625",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Javelin Retail LTD T-A Javelin",
	"Number": "CGB000023",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007842",
		"Description": "Javelin Retail LTD T-A Javelin",
		"email": "biljana@azara-accountants.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gurdeep Sunger",
	"Number": "CGB006033",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "227 Sheffield",
	"Number": "CGB008773",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004078",
		"Description": "227 Sheffield",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mondottica LTD",
	"Number": "CGB000026",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008013",
		"Description": "Mondottica LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB320 Customer",
	"Number": "SCAGB320",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005624",
		"Description": "HOUSE OF FRASER GUILDFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aer Lingus Acc Payable - Po Box 45",
	"Number": "CGB000613",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006799",
		"Description": "Aer Lingus Catering Bond",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Five Retail LTD T-A Porters",
	"Number": "CGB001036",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008212",
		"Description": "Five Retail LTD T-A Porters",
		"email": "bob@g23.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Victoria Faith LTD T-A",
	"Number": "CGB000957",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008475",
		"Description": "Victoria Faith LTD T-A",
		"email": "victoria@womens-society.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hollie Pannell",
	"Number": "CGB003432",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "KIMBERLEY TERESA KING",
	"Number": "CGB009834",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Riccado Ltd",
	"Number": "CGB000096",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008317",
		"Description": "Bector Anil Kumar T-A Riccado",
		"email": "anil@riccado.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Robyn Drew CPS",
	"Number": "CGB002957",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030889",
		"Description": "Robyn Drew CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Zachary Chew",
	"Number": "CGB004038",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Quest Retail LTD T-A Diffusion",
	"Number": "CGB000142",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008258",
		"Description": "Quest Retail LTD T-A Diffusion",
		"email": "carl@diffusion.uk.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wagenaar Mode",
	"Number": "CGB000945",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008399",
		"Description": "Wagenaar Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB920 Customer",
	"Number": "SCAGB920",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005777",
		"Description": "JOHN LEWIS - CAMBRIDGE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abby Wright",
	"Number": "CGB001578",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free2",
	"Number": "CGB010753",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000150316",
		"Description": "Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free2",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": false,
	"errorMessage": "(Validation Error: Account Name[Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free2] is greater than 50 characters)",
	"errorNumber": "ESB001"
},
{
	"AccountName": "GB912 Customer",
	"Number": "SCAGB912",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005766",
		"Description": "JOHN LEWIS - CRIBBS CAUSEWAY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Smart",
	"Number": "CGB005358",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fonnie Tang",
	"Number": "CGB001797",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Signet Trading LTD T-A Ernest Jones",
	"Number": "CGB000124",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007324",
		"Description": "Signet Trading LTD T-A Ernest Jones",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Cameron Freeman-Haynes",
	"Number": "CGB001348",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jack Victor LTD Royalties",
	"Number": "CGB001217",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013055",
		"Description": "JACK VICTOR LTD ROYALTIES",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JD Williams & Co Ltd t/a Figleaves",
	"Number": "CGB000045",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007387",
		"Description": "Figleaves.com Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mrs R Molyneaux",
	"Number": "CGB001025",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008021",
		"Description": "Mrs R Molyneaux",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SC FASHION DAYS SHOPPING SRL",
	"Number": "CGB007271",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000065495",
		"Description": "SC FASHION DAYS SHOPPING SRL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LORNA REOACH",
	"Number": "CGB004104",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031565",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "ALEXANDRA JACKSON",
	"Number": "CGB009337",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000051203",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "OrderUploadPerfTest",
	"Number": "CGB010785",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000150519",
		"Description": "OrderUploadPerfTest",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Myriam Mode",
	"Number": "CGB000956",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008055",
		"Description": "Myriam Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "267 Heathrow T4",
	"Number": "CGB007808",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000012985",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "rachel neylon",
	"Number": "CGB005065",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "YASMEEN MOHAMED",
	"Number": "CGB005828",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Anview LTD T/A Maysons",
	"Number": "CGB000079",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006587",
		"Description": "Anview LTD T/A Maysons",
		"email": "maysons@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Czech - High Tech Logistics",
	"Number": "CGB000692",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006603",
		"Description": "Apple Czech - High Tech Logistics",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "West Bromich Albion Football Club",
	"Number": "CGB000918",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008403",
		"Description": "West Bromich Albion Football Club",
		"email": "andrew.hussey@wbafc.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Arnotts LTD",
	"Number": "CGB000118",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006633",
		"Description": "Arnotts LTD - Deliveries",
		"email": "arnottsapmerch@southwestern.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Scarlett Tippett",
	"Number": "CGB007938",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Zalando Se - SMS INVOICES",
	"Number": "CGB001284",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013135",
		"Description": "ZALANDO SE - SMS INVOICES",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB923 Customer",
	"Number": "SCAGB923",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005781",
		"Description": "JOHN LEWIS - WATFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Surfdome Shop Ltd t/a Surfdome.com",
	"Number": "CGB000340",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008541",
		"Description": "Surfdome @ Norbert Dentressangle",
		"email": "erica.fritz@surfdome.com; amy.kormoss@surfdome.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Handelsonderneming Spandaw",
	"Number": "CGB001318",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018822",
		"Description": "Handelsonderneming Spandaw",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Chris Cromby",
	"Number": "CGB010000",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003304",
		"Description": "Chris Cromby",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rsh Middle East Llc   Royalty",
	"Number": "CGB001200",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013024",
		"Description": "RSH MIDDLE EAST LLC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALEXEY KHMELEV",
	"Number": "CGB001747",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alexa Burton",
	"Number": "CGB003631",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "BETHANY SMITH",
	"Number": "CGB001645",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mrs S Turner T-A Bone",
	"Number": "CGB000149",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008047",
		"Description": "Mrs S Turner T-A Bone",
		"email": "sue@boneclothing.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Newcastle",
	"Number": "CUKCC0071",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149593",
		"Description": "John Lewis - Newcastle",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Richelieu",
	"Number": "CGB000934",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008313",
		"Description": "Richelieu",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Decorum Menswear LTD",
	"Number": "CGB000171",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007204",
		"Description": "Decorum Menswear LTD",
		"email": "decorummensonline@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ryan Stanton",
	"Number": "CGB001561",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "La Vetrina",
	"Number": "CGB000889",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007509",
		"Description": "La Vetrina",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dave Patrick",
	"Number": "CGB002006",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dillards The Mall In St Matthe",
	"Number": "CGB009211",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046404",
		"Description": "Dillards DC 21",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046553",
		"Description": "Dillards The Mall In St Matthe",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jacob Kaliroy CPS",
	"Number": "CGB003238",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030895",
		"Description": "Jacob Kaliroy CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Pelham Leather Goods LTD",
	"Number": "CGB001258",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013113",
		"Description": "PELHAM LEATHER GOODS LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Brand Capital Retail Management",
	"Number": "CGB001280",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006988",
		"Description": "Brand Capital Retail Management",
		"email": "storm@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "That Bournemouth Big Hotel Ltd t/a Hilton Bournemouth",
	"Number": "CGB008705",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044644",
		"Description": "That Bournemouth Big Hotel Ltd t/a Hilton Bournemouth",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": false,
	"errorMessage": "(Validation Error: Account Name[That Bournemouth Big Hotel Ltd t/a Hilton Bournemouth] is greater than 50 characters)",
	"errorNumber": "ESB001"
},
{
	"AccountName": "Vip Accounts - Clare Rk",
	"Number": "CGB000030",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008651",
		"Description": "Vip Accounts - Clare Rk",
		"email": "clare.ryan-kavanagh@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Catch",
	"Number": "CGB001150",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007163",
		"Description": "Catch",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jasmine Rhoden",
	"Number": "CGB007940",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Emma Moon",
	"Number": "CGB001415",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Marika Soluk",
	"Number": "CGB003557",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB270 Stansted",
	"Number": "CGB003982",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031250",
		"Description": "GB270 Stansted",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Calvin Tapfumaneyi",
	"Number": "CGB003671",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "W D Coe LTD T-A Goddards",
	"Number": "CGB000041",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006946",
		"Description": "W D Coe LTD T-A Goddards",
		"email": "info@goddardsonline.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "harley Horsley",
	"Number": "CGB005632",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045603",
		"Description": "harley horsleyDONOTU",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Brand Capital - Bulk",
	"Number": "CGB001082",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006937",
		"Description": "Stuttafords Gabarone",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000006944",
		"Description": "Stuttafords Gateway",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000006985",
		"Description": "Brand Capital - Bulk",
		"email": "storm@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000006992",
		"Description": "Brand Capital - Bulk",
		"email": "storm@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007099",
		"Description": "Stuttafords Claremont",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007101",
		"Description": "Stuttafords Clearwater",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007175",
		"Description": "Stuttafords Canal Walk",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007282",
		"Description": "Stuttafords Eastgate",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007856",
		"Description": "Stuttafords Sandton",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007952",
		"Description": "Stuttafords Menlyn",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008063",
		"Description": "Brand Capital Sa - Natural Classics",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008150",
		"Description": "Stuttafords Pavillion",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008260",
		"Description": "Brand Capital Sa - Queens",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008453",
		"Description": "Stuttafords Windhoek",
		"email": "marco@brandcapital.co.za",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB335 Customer",
	"Number": "SCAGB335",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005618",
		"Description": "HOUSE OF FRASER MANCHESTER’",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sara Ridouane",
	"Number": "CGB001646",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Joanne Dotters",
	"Number": "CGB001567",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Repertoire Beaconsfield",
	"Number": "CGB000113",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008277",
		"Description": "Repertoire Beaconsfield",
		"email": "beverleygough@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Torran Caulfield",
	"Number": "CGB008608",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "254 White City 1",
	"Number": "CGB008511",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003011",
		"Description": "254 White City 1",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lucy Anderson",
	"Number": "CGB003495",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003720",
		"Description": "Lucy Parkin",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Geneva Watch Group Inc",
	"Number": "CGB001206",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013037",
		"Description": "GENEVA WATCH GROUP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Martina Kvasnovska",
	"Number": "CGB001584",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Cheadle",
	"Number": "CUKCC0055",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149577",
		"Description": "John Lewis - Cheadle",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Stevie Robinson",
	"Number": "CGB005054",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Yasmin Graham",
	"Number": "CGB003231",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "shemei wright",
	"Number": "CGB008637",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Winkeler",
	"Number": "CGB000967",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008455",
		"Description": "Winkeler",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aisling Uniacke FAF",
	"Number": "CGB005502",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034994",
		"Description": "Aisling Uniacke FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Leeds",
	"Number": "CUKCC0089",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149381",
		"Description": "STGB - JOHN LEWIS - CHELMSFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Danielle Marshall",
	"Number": "CGB010340",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Oakley Court Hotel",
	"Number": "CGB001093",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008100",
		"Description": "Oakley Court Hotel",
		"email": "sean.gleeson@oakleycourt.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Cardiff",
	"Number": "CUKCC0082",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149604",
		"Description": "John Lewis - Cardiff",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Holly Jenkins",
	"Number": "CGB010171",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037138",
		"Description": "holly jenkins",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB909 Customer",
	"Number": "SCAGB909",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005772",
		"Description": "JOHN LEWIS - KINGSTON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Jet2 Edinburgh",
	"Number": "CGB000915",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007635",
		"Description": "Alpha Lsg LTD - Jet2 Edinburgh",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bastiaansen Modestad",
	"Number": "CGB001132",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006689",
		"Description": "Bastiaansen Modestad",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "callum Yew",
	"Number": "CGB005057",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033738",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alex Boutique",
	"Number": "CGB000736",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006872",
		"Description": "Alex Boutique",
		"email": "administracio@alexboutiques.com; maria.alexboutique@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Proporta LTD -",
	"Number": "CGB001211",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013047",
		"Description": "PROPORTA LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Edels Boutique",
	"Number": "CGB000052",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007288",
		"Description": "Edels Boutique",
		"email": "edelhanly@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Nicol",
	"Number": "CGB004300",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Maribel Fernandez SL",
	"Number": "CGB005839",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034413",
		"Description": "Maribel Fernandez SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Blok Mode",
	"Number": "CGB000822",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006753",
		"Description": "Blok Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bentalls - Bracknell",
	"Number": "CUKCC0044",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149566",
		"Description": "Bentalls - Bracknell",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ROSS BRIERS",
	"Number": "CGB002671",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031496",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "HOF - Bristol",
	"Number": "CUKCC0020",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149542",
		"Description": "HOF - Bristol",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Boros Leathergoods LTD T-A Boros",
	"Number": "CGB000104",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006975",
		"Description": "Boros Leathergoods LTD T-A Boros",
		"email": "borosaccounts@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Karina Bingham",
	"Number": "CGB006174",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "INANA KONACIC F&F",
	"Number": "CGB010251",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000112032",
		"Description": "INANA KONACIC F&F",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Solihull",
	"Number": "CUKCC0078",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149600",
		"Description": "John Lewis - Solihull",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Beauty Spot",
	"Number": "CGB001236",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013083",
		"Description": "THE BEAUTY SPOT",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Voulez Vous Ludlow LTD",
	"Number": "CGB000636",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008375",
		"Description": "Voulez Vous Ludlow LTD",
		"email": "vickiowen1@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB906 Customer",
	"Number": "SCAGB906",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005760",
		"Description": "JOHN LEWIS - BRENT CROSS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "nicole McIver",
	"Number": "CGB008855",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Noa Noa For Women",
	"Number": "CGB000833",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008070",
		"Description": "Noa Noa For Women",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "270 Stansted",
	"Number": "CGB003987",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031465",
		"Description": "GB270 Stansted",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jamie Marshall",
	"Number": "CGB009051",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hue",
	"Number": "CGB000827",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007785",
		"Description": "Hue",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lorena Marco Garcia t/a Piccolo Gioie",
	"Number": "CGB007451",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041197",
		"Description": "ena Marco Garcia t/a Piccolo Gioie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "W J Daniels And Company LTD",
	"Number": "CGB000193",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007187",
		"Description": "W J Daniels And Company LTD",
		"email": "margaretrynne@danielstores.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "matthew best",
	"Number": "CGB007947",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ste Expl Des Ets Smart T-A Smart",
	"Number": "CGB000952",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007908",
		"Description": "Ste Expl Des Ets Smart T-A Smart",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB618 Customer",
	"Number": "CGB007860",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "COMMERCIO EXCELENTE NORTE SUR SA DE CV",
	"Number": "CGB007155",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039755",
		"Description": "COMERCIO EXCELENTE NORTE SUR SA DE CV",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alicia Badcock",
	"Number": "CGB001769",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Van Mildert Lifestyle LTD",
	"Number": "CGB001066",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008685",
		"Description": "Van Mildert Lifestyle LTD",
		"email": "debbie@oxynet.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Osama Shubir",
	"Number": "CGB010574",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dane Matthews",
	"Number": "CGB001533",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Birmingham",
	"Number": "CGB000602",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006808",
		"Description": "Alpha Flight Uk LTD - Tcx Bhx",
		"email": "dseymour@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Erka Boldbaatar",
	"Number": "CGB008288",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Attica Department Stores SA",
	"Number": "CGB001121",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008629",
		"Description": "Attica-Transcombi Expr Salonico 3pl",
		"email": "svarna@atticadps.gr; fragou@atticadps.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "James Harragan",
	"Number": "CGB002619",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Slater Mens Wear",
	"Number": "CGB000015",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007906",
		"Description": "Slater Mens Wear (Warehouse)",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Food Development @ Trading LTD",
	"Number": "CGB000737",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008199",
		"Description": "Food Development @ Trading LTD",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB318 Customer",
	"Number": "SCAGB318",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005615",
		"Description": "HOUSE OF FRASER CARDIFF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB242 Customer",
	"Number": "SCAGB242",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005570",
		"Description": "ASHFORD - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Robyn Jones",
	"Number": "CGB010398",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Adam Millington",
	"Number": "CGB003421",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Paul's LTD",
	"Number": "CGB001039",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007469",
		"Description": "Pauls LTD",
		"email": "hugh@paulskilkenny.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Gatwick",
	"Number": "CGB000251",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006832",
		"Description": "Alpha Lsg LTD",
		"email": "daniel.szynalik@alphalsg.co.uk; marcin.zwada@alphalsg.co.uk; ingrida.zitkute@alphalsg.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "FENWICKS & BENTALLS - EDI Acc",
	"Number": "CUKFW1000",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149887",
		"Description": "FENWICKS & BENTALLS - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aditya Birla Fashion & Retail Ltd",
	"Number": "CGB000240",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008041",
		"Description": "Aditya Birla Fashion & Retail Ltd",
		"email": "kumar.k@madura.aditabirla.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "265 Liverpool",
	"Number": "CGB003810",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002595",
		"Description": "265 Liverpool",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Thomas Rosbotham",
	"Number": "CGB003451",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Cia de la Moda",
	"Number": "CGB001684",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024445",
		"Description": "Cia De La Moda",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abby Howells",
	"Number": "CGB005807",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jordan Bethell",
	"Number": "CGB002565",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jaslin Kenejah",
	"Number": "CGB001732",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "244 Bath",
	"Number": "CGB008165",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002964",
		"Description": "244 Bath",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ross Carroll",
	"Number": "CGB001422",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035007",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarah Hogan",
	"Number": "CGB003629",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jackson And Son LTD T-A Sidewalk",
	"Number": "CGB000074",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007884",
		"Description": "Jackson And Son LTD T-A Sidewalk",
		"email": "sidewalk-st.helens@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
	"Number": "CGB001054",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008178",
		"Description": "Peek And Cloppenburg Vienna Buying",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abby Griffiths",
	"Number": "CGB009069",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB247 Customer",
	"Number": "SCAGB247",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005577",
		"Description": "BROMPTON ROAD – TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mrs L Edmonds",
	"Number": "CGB001028",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007549",
		"Description": "Mrs L Edmonds",
		"email": "0772 309 4505 mr edmonds",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sasfin Asia LTD C-O Brand Capital",
	"Number": "CGB000639",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006987",
		"Description": "Sasfin Asia LTD C-O Brand Capital",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free",
	"Number": "CGB001273",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006795",
		"Description": "Aelia Duty Free - The Fashion Place",
		"email": "mthompson@lstruk.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": false,
	"errorMessage": "(Validation Error: Account Name[Lagardere Travel Retail [UK] Ltd t/a Aelia Duty Free] is greater than 50 characters)",
	"errorNumber": "ESB001"
},
{
	"AccountName": "Emma Round",
	"Number": "CGB010061",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "HOF - Edinburgh (Closed)",
	"Number": "CUKCC0011",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149533",
		"Description": "HOF - Edinburgh (Closed)",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Becky Parmar FAF",
	"Number": "CGB004918",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035518",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB950 Customer",
	"Number": "SCAGB950",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005792",
		"Description": "JOHN LEWIS - ONLINE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Grace Tucker",
	"Number": "CGB001792",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "JOHN LEWIS - EDI Acc",
	"Number": "CUKJL1000",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149890",
		"Description": "JOHN LEWIS - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ryan Sweeney",
	"Number": "CGB009293",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "S And Ps Fashions LTD T-A Kular",
	"Number": "CGB001047",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007501",
		"Description": "S And Ps Fashions LTD T-A Kular",
		"email": "harjesh@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "virginia Ridolfi",
	"Number": "CGB001349",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB219 Customer",
	"Number": "SCAGB219",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005552",
		"Description": "CANARY WHARF - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laura McCabe",
	"Number": "CGB001580",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031410",
		"Description": "laura mccabe FAF",
		"email": "246.gatwicknorth@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Pt Gagan Indonesia",
	"Number": "CGB001233",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013079",
		"Description": "PT GAGAN INDONESIA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Castromont Bierzo t/a BTQ Cherie",
	"Number": "CGB003533",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029197",
		"Description": "Castromont Bierzo t/a BTQ Cherie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Norton Barrie Ltd",
	"Number": "CGB007441",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041049",
		"Description": "Norton Barrie Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sportsdirect.com Retail Ltd t/a Cruise Clothing",
	"Number": "CGB006668",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037710",
		"Description": "Cruise Clothing Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Simply Scuba LTD T-A Simply Beach",
	"Number": "CGB000640",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008625",
		"Description": "Simply Scuba LTD T-A Simply Beach",
		"email": "jodennis@simplybeach.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB334 Customer",
	"Number": "SCAGB334",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005638",
		"Description": "HOUSE OF FRASER CHELTENHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Madina Boutique",
	"Number": "CGB000844",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007658",
		"Description": "Madina Boutique",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hetabel Ltd t/a Cottrills Rewards",
	"Number": "CGB007032",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000038328",
		"Description": "Hetabel LTD t/a Cottrills Reward",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "All The Queens Men",
	"Number": "CGB000963",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006667",
		"Description": "All The Queens Men",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amelia Richards",
	"Number": "CGB005572",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [Newcastle]",
	"Number": "CGB000138",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007378",
		"Description": "Fenwick [NCL] Wholesale Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000054895",
		"Description": "SEPT 2017 Fenwick [NCL] ONLINE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Beatrice Faldon",
	"Number": "CGB006027",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Oscar Edwards",
	"Number": "CGB001635",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hajerah Malik",
	"Number": "CGB003235",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sonam Sharma",
	"Number": "CGB003617",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Atelier Des Createurs",
	"Number": "CGB001042",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007143",
		"Description": "Atelier Des Createurs",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "calum coulter",
	"Number": "CGB003175",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Velvet",
	"Number": "CGB000866",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008639",
		"Description": "Velvet",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amy Wragg CPS",
	"Number": "CGB009850",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000099786",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "marta walus",
	"Number": "CGB005907",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mandy Catherine McDowell",
	"Number": "CGB005918",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wgab LTD T-A Excel Clothing",
	"Number": "CGB000056",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007346",
		"Description": "Wgab LTD T-A Excel Clothing",
		"email": "william@excelclothing.co.uk; alison@excelclothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "STEPH MCGRATH",
	"Number": "CGB001770",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ascension Calderon Espadas t/a Buber",
	"Number": "CGB005835",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034281",
		"Description": "Ascension Calderon Espadas t/a Buber",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "amy a parnham",
	"Number": "CGB010170",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hope Clothing LTD",
	"Number": "CGB000108",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007722",
		"Description": "Hope Clothing LTD",
		"email": "royston - hopeaylesbury@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Viewstyle LTD T-A View [Hereford]",
	"Number": "CGB000117",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008647",
		"Description": "Viewstyle LTD T-A View [Hereford]",
		"email": "becky.rumsey@btopenworld.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Anne Belle Mode",
	"Number": "CGB001136",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006779",
		"Description": "Anne Belle Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges Retail Ltd",
	"Number": "CGB007606",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041609",
		"Description": "Selfridges Deliveries - Exel",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Peterborough",
	"Number": "CUKCC0059",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149581",
		"Description": "John Lewis - Peterborough",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Giulia Morello",
	"Number": "CGB005119",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Michela De Santes",
	"Number": "CGB008579",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB931 Customer",
	"Number": "SCAGB931",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005790",
		"Description": "JOHN LEWIS - EXETER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Josh Stonelake",
	"Number": "CGB004761",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sasha Tunnicliffe",
	"Number": "CGB010591",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jonathan Arundale",
	"Number": "CGB003948",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "ECO Associates Worldwide Ltd",
	"Number": "CGB007444",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041054",
		"Description": "ECO Associates Worldwide Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Barrowvig LTD t/a Ryles",
	"Number": "CGB003189",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000012977",
		"Description": "Ryles",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LLC BIT t/a Names",
	"Number": "CGB000771",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006351",
		"Description": "LLC BIT t/a Names",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alessia Marini",
	"Number": "CGB003968",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Marjolynn LTD T-A Chantilly",
	"Number": "CGB000185",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007073",
		"Description": "Marjolynn LTD T-A Chantilly",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Simon Harrison Limited",
	"Number": "CGB001226",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013068",
		"Description": "SIMON HARRISON LIMITED",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Debenhams Retail PLC - Royalty",
	"Number": "CGB001184",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013000",
		"Description": "DEBENHAMS RETAIL PLC - ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Gateshead",
	"Number": "CUKCC0010",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149532",
		"Description": "HOF - Gateshead",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Albion 1879 - 338b Pto Banus",
	"Number": "CGB000748",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006771",
		"Description": "Albion 1879 - 338b Pto Banus",
		"email": "nigel@albionspain.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aria Boutique LTD T-A Aria Boutique",
	"Number": "CGB000863",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006629",
		"Description": "Aria Boutique LTD T-A Aria Boutique",
		"email": "karolynn@ariaboutique.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ws Pettit And Co LTD [Brid]",
	"Number": "CGB000110",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008152",
		"Description": "Ws Pettit And Co LTD [Brid]",
		"email": "paul@pettits.com; donna@pettits.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB616 Customer",
	"Number": "SCAGB616",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045981",
		"Description": "STGB616 - TEDCOMM - NETHERLANDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hibaaq Hersi",
	"Number": "CGB001370",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Baron And Baronessa",
	"Number": "CGB001165",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006685",
		"Description": "Baron And Baronessa",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kayleigh Elbrow CPS",
	"Number": "CGB003202",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030892",
		"Description": "Kayleigh Elbrow CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ryan Muncey",
	"Number": "CGB007671",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB246 Customer",
	"Number": "SCAGB246",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005575",
		"Description": "GATWICK NORTH TERMINAL - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "R K Lloyd Of Eccleshall LTD",
	"Number": "CGB000852",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008643",
		"Description": "R K Lloyd Of Eccleshall LTD T-A Veuve",
		"email": "sales@veuveshoes.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "215 Livingston",
	"Number": "CGB004772",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000000658",
		"Description": "215 Livingston",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Glosale LTD T-A Chequers",
	"Number": "CGB000059",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007089",
		"Description": "Glosale LTD T-A Chequers",
		"email": "sales@chequersmenswear.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Encore Retail LTD T-A Encore",
	"Number": "CGB000080",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007318",
		"Description": "Encore Retail LTD T-A Encore",
		"email": "paul@encoreretail.co.uk; john@encoreretail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bellucci",
	"Number": "CGB000782",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006757",
		"Description": "Bellucci",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rebecca Torn",
	"Number": "CGB002398",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "ELLIOT SEXTON",
	"Number": "CGB002175",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035159",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wildfire Entertainment LTD",
	"Number": "CGB000849",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008425",
		"Description": "Oblix Restaurant",
		"email": "marion@zumarestaurant.com; nina@zumarestaurant.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Clare Bibb",
	"Number": "CGB001855",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - York",
	"Number": "CUKCC0048",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149570",
		"Description": "Fenwicks - York",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "PMG Investments Ltd t/a Galvin for Men",
	"Number": "CGB001272",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006938",
		"Description": "PMG Investments Ltd t/a Galvin for Men",
		"email": "info@galvinformen.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fraser Humphries",
	"Number": "CGB001918",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031516",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "That Bournemouth Big Hotel Ltd",
	"Number": "CGB005087",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036151",
		"Description": "That Bournemouth Big Hotel Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB262 Customer Westfield Tennis Pop-Up",
	"Number": "CGB001374",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000145081",
		"Description": "TED BAKER TENNIS POPUP - WHITE CITY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Cribbs Causeway",
	"Number": "CUKCC0066",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149588",
		"Description": "John Lewis - Cribbs Causeway",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "George Stylianou t/a Cult",
	"Number": "CGB000086",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007113",
		"Description": "Cult",
		"email": "info@cultclothingn8.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB919 Customer",
	"Number": "SCAGB919",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005776",
		"Description": "JOHN LEWIS - LEICESTER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Psyche LTD",
	"Number": "CGB000037",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008228",
		"Description": "Psyche LTD",
		"email": "sue@psyche.co.uk; helen@psyche.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Katie Dunning",
	"Number": "CGB001920",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bullitt Audio Ltd",
	"Number": "CGB001262",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013119",
		"Description": "BULLITT GROUP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nichola Kingsury T-A Se7en -Mens",
	"Number": "CGB000197",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008361",
		"Description": "Nichola Kingsury T-A Se7en -Mens",
		"email": "no email",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Web Shop Direct Ltd t/a UK Swimwear",
	"Number": "CGB008793",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044708",
		"Description": "Web Shop Direct Ltd t/a UK Swimwear",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mr Sc Robson T-A Visual Impact",
	"Number": "CGB000048",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008661",
		"Description": "Visual Impact Delivery Address",
		"email": "office@visualimpact.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Asos.com Ltd Head Office SMS",
	"Number": "CGB000121",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006659",
		"Description": "Asos.Com LTD - Sms Code",
		"email": "accounts@asos.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "225 Braintree",
	"Number": "CGB009004",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004728",
		"Description": "225 Braintree",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jettys",
	"Number": "CGB001125",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007844",
		"Description": "Jettys",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jessica Dawson",
	"Number": "CGB005817",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB345 Customer",
	"Number": "SCAGB345",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005641",
		"Description": "HOUSE OF FRASER BLUEWATER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SWISS FASHION TIME GMBH",
	"Number": "CGB007071",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000038577",
		"Description": "SWISS FASHION TIME GMBH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jessica Wright",
	"Number": "CGB002744",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB305 Customer",
	"Number": "SCAGB305",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005605",
		"Description": "SELFRIDGES – MANCHESTER EXCHANGE SQUARE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "TGR RETAIL LTD - Teds Grooming",
	"Number": "CGB001277",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007292",
		"Description": "TGR RETAIL LTD",
		"email": "niyazi@nyoaccountants.com; mus@tedsgroomingroom.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Zalando SE - GBP",
	"Number": "CGB006664",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037573",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Online",
	"Number": "CUKCC0090",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149612",
		"Description": "John Lewis - Online",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Enclothed Limited",
	"Number": "CGB001143",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033452",
		"Description": "Enclothed Limited",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ELYS - EDI Acc",
	"Number": "CUKEL1000",
	"Group": "R-CON-ELY",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149886",
		"Description": "ELYS - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Arddi Crescini",
	"Number": "CGB001363",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Melissa Richardson",
	"Number": "CGB003282",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sean Worrall",
	"Number": "CGB003317",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Grangemuir Associates T-A A Bientot",
	"Number": "CGB000618",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006783",
		"Description": "Grangemuir Associates T-A A Bientot",
		"email": "norriestewart@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Riva Menswear Ltd",
	"Number": "CGB008779",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044843",
		"Description": "Riva Menswear Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charley Jones - PU",
	"Number": "CGB001629",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030460",
		"Description": "Charley Jones - PU",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB207 Customer",
	"Number": "SCAGB207",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005543",
		"Description": "YORK - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lifestyle Stirling LTD",
	"Number": "CGB000571",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008066",
		"Description": "Lifestyle Stirling LTD",
		"email": "keith@numbereightclothing.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Leeds",
	"Number": "CUKCC0037",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149559",
		"Description": "HOF - Leeds",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Emine Ibishi",
	"Number": "CGB009214",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046892",
		"Description": "DON'T USE THIS ACC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nicola Collins",
	"Number": "CGB007885",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB372 Customer",
	"Number": "SCAGB372",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005647",
		"Description": "FENWICK CANTERBURY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rogelio Amaral",
	"Number": "CGB008977",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Joanne Shephard",
	"Number": "CGB003392",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hugh Harris LTD",
	"Number": "CGB000067",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007789",
		"Description": "Hugh Harris LTD",
		"email": "shop@hughharris.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nagajan Modhwadia CPS",
	"Number": "CGB003250",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030886",
		"Description": "Nagajan ModhwadiaCPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Manuel Cerdan Quero t/a BTQ Atelier",
	"Number": "CGB003205",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026524",
		"Description": "Manuel Cerdan Quero t/a BTQ Atelier",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Baco SRL - SMS",
	"Number": "CGB008304",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043610",
		"Description": "Baco Distribution SRL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Chloe Samain",
	"Number": "CGB007859",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042887",
		"Description": "CHLOE SAMAIN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB330 Customer",
	"Number": "SCAGB330",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005634",
		"Description": "HOUSE OF FRASER HUDDERSFIELD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "DVV Media Group GMBH",
	"Number": "CGB001299",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018732",
		"Description": "DVV Media Group GMBH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Repertoire Salisbury",
	"Number": "CGB000114",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008345",
		"Description": "Repertoire Salisbury",
		"email": "beverleygough@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB211 Customer",
	"Number": "SCAGB211",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005544",
		"Description": "SWINDON - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "No Ordinary Shoes Ltd USD Royalty",
	"Number": "CGB001190",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013009",
		"Description": "NO ORDINARY SHOES LTD USD ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB253 Customer",
	"Number": "SCAGB253",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005583",
		"Description": "BRISTOL - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kingsley Worrall",
	"Number": "CGB005329",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lexie Elliott",
	"Number": "CGB001534",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB327 Customer",
	"Number": "SCAGB327",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005629",
		"Description": "HOUSE OF FRASER NOTTINGHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "William Major LTD",
	"Number": "CGB000091",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007990",
		"Description": "William Major LTD",
		"email": "derek.mccormack47@yahoo.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB316 Customer",
	"Number": "SCAGB316",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005613",
		"Description": "HOUSE OF FRASER VICTORIA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Louis Copeland And Sons LTD",
	"Number": "CGB001110",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007537",
		"Description": "Louis Copeland And Sons LTD",
		"email": "james@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB239 Customer",
	"Number": "SCAGB239",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005567",
		"Description": "GUILDFORD - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000005592",
		"Description": "WESTFIELD - TED BAKER PASHION",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "De Windt",
	"Number": "CGB001313",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018851",
		"Description": "De Windt",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sikandar Khan",
	"Number": "CGB001638",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hannah Foster",
	"Number": "CGB005320",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Killow Retail LTD T-A Willow",
	"Number": "CGB000941",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007481",
		"Description": "Killow Retail LTD T-A Willow",
		"email": "jean@willow.ie; willowfashion@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB009644",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000065867",
		"Description": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nicole Salter",
	"Number": "CGB005927",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Esther Cendon Carballeda t/a Frisee",
	"Number": "CGB007448",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041170",
		"Description": "Esther Cendon Carballeda t/a Frisee",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Pilar Molina Martinez",
	"Number": "CGB008997",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045363",
		"Description": "Pilar Molina Martinez",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Victoria",
	"Number": "CUKCC0012",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149534",
		"Description": "HOF - Victoria",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Valbona Litja",
	"Number": "CGB007713",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005109",
		"Description": "Valbona Litja",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "A & J Tattersall Ltd t/a DB3",
	"Number": "CGB001359",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024061",
		"Description": "A & J Tattersall Ltd t/a DB3",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB650 Customer",
	"Number": "CGB010584",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000145173",
		"Description": "GB650 Customer",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Nottingham",
	"Number": "CUKCC0023",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149545",
		"Description": "HOF - Nottingham",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Roberto Gerrards LTD",
	"Number": "CGB000097",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008333",
		"Description": "Roberto Gerrards LTD",
		"email": "robertogerrards@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Savoy Hotel",
	"Number": "CGB001103",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008545",
		"Description": "The Savoy Hotel",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Megan Magenis",
	"Number": "CGB005108",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hope Clothing LTD",
	"Number": "CGB000153",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007759",
		"Description": "Hope Clothing LTD",
		"email": "raheelnakhwa@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hamad And Mohammed Al Futtaim",
	"Number": "CGB001266",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013124",
		"Description": "HAMAD AND MOHAMMED AL FUTTAIM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Katarzyna Korzonkiewcz",
	"Number": "CGB001786",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Georgia Thomas",
	"Number": "CGB003015",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB008116",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043145",
		"Description": "Peek and Cloppenburg Retail Buying Gmbh & Co KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Little Green Bag",
	"Number": "CGB001070",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007573",
		"Description": "The Little Green Bag",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Chelsea Preston",
	"Number": "CGB005916",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Edward Donnellan Co Ltd",
	"Number": "CGB007601",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041384",
		"Description": "Edward Donnellan Co Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Im Barry T-A Cockney Rebel",
	"Number": "CGB000125",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007139",
		"Description": "Im Barry T-A Cockney Rebel",
		"email": "cockneyrebel1@live.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Banstead Store LTD T-A Fluke",
	"Number": "CGB000192",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007397",
		"Description": "The Banstead Store LTD T-A Fluke",
		"email": "peter@flukebanstead.com; kirsti@flukebanstead.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Claire Baker",
	"Number": "CGB004197",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB612 Customer",
	"Number": "SCAGB612",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045976",
		"Description": "STGB612 - TEDCOMM - NORTH EU",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis Statements",
	"Number": "CGB000006",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007421",
		"Description": "John Lewis Statements",
		"email": "jl_invoice_queries@partnershipservices.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Grant Green",
	"Number": "CGB001421",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003644",
		"Description": "Grant Green CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - York",
	"Number": "CUKCC0086",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149608",
		"Description": "John Lewis - York",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB915 Customer",
	"Number": "SCAGB915",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005770",
		"Description": "JOHN LEWIS - ABERDEEN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Herenkleding Groningen bv",
	"Number": "CGB001324",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018869",
		"Description": "Herenkleding",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Adam Keith",
	"Number": "CGB001411",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004748",
		"Description": "Adam Keith",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Silk Emporium LTD T-A Silk",
	"Number": "CGB000268",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007896",
		"Description": "Silk Emporium LTD T-A Silk",
		"email": "aine.silk@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Leah Ridge FAF",
	"Number": "CGB002852",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030485",
		"Description": "Leah Ridge FAF",
		"email": "FAF",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Top Jills",
	"Number": "CGB000838",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008619",
		"Description": "Top Jills",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Mill Shop LTD",
	"Number": "CGB000175",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007972",
		"Description": "The Mill Shop LTD",
		"email": "jan.doyle@themillshop.co.im",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J E Beale PLC 03 Poole",
	"Number": "CGB000132",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006981",
		"Description": "J E Beale PLC 03 Poole",
		"email": "hpeyman@beales.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wardow GmbH",
	"Number": "CGB008876",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045118",
		"Description": "Wardow GmbH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Attica Department Stores Sa",
	"Number": "CGB000715",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006671",
		"Description": "Attica Department Stores Sa",
		"email": "paleochoriti@atticadps.gr; fragou@atticadps.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bradley Ayers",
	"Number": "CGB003232",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Malena",
	"Number": "CGB000034",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007918",
		"Description": "Malena",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "James McDonnell t/a McDonnell Menswear",
	"Number": "CGB007442",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041047",
		"Description": "James McDonnell t/a McDonnell Menswear",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB264 Customer",
	"Number": "SCAGB264",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005598",
		"Description": "GATWICK SOUTH TERMINAL - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Allie Bruce",
	"Number": "CGB007951",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002357",
		"Description": "Allie Bruce",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Allure",
	"Number": "CGB000760",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006860",
		"Description": "Allure",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Birmingham",
	"Number": "CUKCC0008",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149530",
		"Description": "HOF - Birmingham",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Lingerie Boutique Ltd (REIGATE)",
	"Number": "CGB003499",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027743",
		"Description": "The Lingerie Boutique",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Soraya McGregor",
	"Number": "CGB007944",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB341 Customer",
	"Number": "SCAGB341",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005623",
		"Description": "HOUSE OF FRASER EDINBURGH JENNERS’",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [T-Wells]",
	"Number": "CGB000164",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006922",
		"Description": "Fenwick LTD [T-Wells]",
		"email": "richardmoon@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Specter Associates Ltd t/a Banana Connection",
	"Number": "CGB006730",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037604",
		"Description": "Specter Associates Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Carlisle",
	"Number": "CUKCC0019",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149541",
		"Description": "HOF - Carlisle",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Megan Lamprell DO NOT USE",
	"Number": "CGB002890",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sas Signis T-A Tandem",
	"Number": "CGB001154",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008551",
		"Description": "Sas Signis T-A Tandem",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Payam Abdollahzadeh",
	"Number": "CGB003686",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sam Churchill",
	"Number": "CGB005550",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Harding Brothers Shipping Contract.",
	"Number": "CGB000890",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008383",
		"Description": "Harding Brothers Shipping Contract.",
		"email": "vanessa.fang@hardingbros.co.uk; bookingin@hardingbros.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Leonardo",
	"Number": "CGB001020",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007561",
		"Description": "Leonardo",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Asos.Com T-A Asos.Com LTD china",
	"Number": "CGB001014",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006655",
		"Description": "Asos.Com Deliveries",
		"email": "asoscollection@wtprima.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "London City Club",
	"Number": "CGB000946",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007597",
		"Description": "London City Club",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB350 Customer",
	"Number": "SCAGB350",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005643",
		"Description": "BENTALLS KINGSTON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "James Roberts MISC",
	"Number": "CGB005039",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "247 Brompton Road",
	"Number": "CGB004186",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000004254",
		"Description": "247 Brompton Road",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB926 Customer",
	"Number": "SCAGB926",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005784",
		"Description": "JOHN LEWIS - HIGH WYCOMBE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Girdlestone DO NOT USE",
	"Number": "CGB003556",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Harry Polloway",
	"Number": "CGB001572",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Louis Copeland And Sons LTD",
	"Number": "CGB001114",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007529",
		"Description": "Louis Copeland And Sons LTD",
		"email": "adrian@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dpm Holding Sa T-A Grandezza",
	"Number": "CGB000935",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007254",
		"Description": "Dpm Holding Sa T-A Grandezza",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Luton",
	"Number": "CGB000654",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006838",
		"Description": "Alpha Flight Services - Bond",
		"email": "posmond@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Joshua Howard",
	"Number": "CGB005762",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Eleanor Phoenix",
	"Number": "CGB003408",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Selfridges - Birmingham",
	"Number": "CUKCC0004",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149526",
		"Description": "Selfridges - Birmingham",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jessica Nix",
	"Number": "CGB002155",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird [Jersey] LTD",
	"Number": "CGB000167",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006910",
		"Description": "Ashworth And Bird [Jersey] LTD",
		"email": "neil@ashworthandbird.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aaron Mandalia",
	"Number": "CGB005090",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "O.C. Tanner LTD",
	"Number": "CGB000633",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008104",
		"Description": "O C Tanner LTD",
		"email": "hayley.broadway@octanner.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Georgia Davies",
	"Number": "CGB003343",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Zalando Se",
	"Number": "CGB001095",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008479",
		"Description": "Zalando Se - Mens Acc",
		"email": "service@zalando.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008481",
		"Description": "Zalando Se - Wom Acc",
		"email": "accounting@zalando.de",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008485",
		"Description": "Zalando Se  - Mens",
		"email": "service@zalando.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008495",
		"Description": "Zalando SE Womens",
		"email": "service@zalando.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "No Ordinary Shoes Ltd GBP Royalt",
	"Number": "CGB001189",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013007",
		"Description": "No Ordinary Shoes Ltd GBP ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Adam Francis Moon",
	"Number": "CGB001583",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "emily stone",
	"Number": "CGB010519",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Neiman Marcus Northpark",
	"Number": "CGB010260",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046434",
		"Description": "Neiman Marcus 091-NSC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046500",
		"Description": "Neiman Marcus Northpark",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Natalie Cumming",
	"Number": "CGB001785",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035144",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Chelmsford",
	"Number": "CUKCC0088",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149380",
		"Description": "STGB - JOHN LEWIS - LEEDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB260 Customer",
	"Number": "SCAGB260",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000019147",
		"Description": "GB260 Customer",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges - London",
	"Number": "CUKCC0001",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149523",
		"Description": "Selfridges - London",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Philip Browne",
	"Number": "CGB000002",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008196",
		"Description": "Philip Browne",
		"email": "accounts@philipbrownemenswear.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Southsea",
	"Number": "CUKCC0083",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149605",
		"Description": "John Lewis - Southsea",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alyshea Jackson",
	"Number": "CGB003386",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB310 Customer",
	"Number": "SCAGB310",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005635",
		"Description": "HOF WEB STOCK - STORE 1097",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD  - Ls1 Gatwick",
	"Number": "CGB000249",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007615",
		"Description": "Alpha Lsg LTD - Gatwick",
		"email": "daniel.szynalik@alphalsg.co.uk; marcin.zwada@alphalsg.co.uk;  ingrida.zitkute@alphalsg.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Moss Bros Group PLC Formal",
	"Number": "CGB000007",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008033",
		"Description": "Moss Bros Distribution Centre",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mr And Mrs Mc LTD T-A Neola Dundalk",
	"Number": "CGB000933",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007998",
		"Description": "Mr And Mrs Mc LTD Ta Neola Malahide",
		"email": "accounts@neola.ie",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008000",
		"Description": "Mr And Mrs Mc LTD T-A Neola Dundalk",
		"email": "accounts@neola.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nicola Smith",
	"Number": "CGB002149",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Elliotts Retailing Ltd",
	"Number": "CGB003811",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030266",
		"Description": "Elliotts Retailing Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ra Johnson And Di Johnson T-A",
	"Number": "CGB000145",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007413",
		"Description": "Ra Johnson And Di Johnson T-A",
		"email": "davidjohnson8@virginmedia.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mankind Lichfield LTD T-A Mankind",
	"Number": "CGB001035",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007922",
		"Description": "Mankind Lichfield LTD T-A Mankind",
		"email": "juliemankind@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gate Gourmet London LTD",
	"Number": "CGB001281",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006954",
		"Description": "Gate Gourmet LTD",
		"email": "lmoore@gategourmet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Philippa Layzell",
	"Number": "CGB002007",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Flybe Limited",
	"Number": "CGB000579",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006898",
		"Description": "Commissaire Bonded Stores [Flybe]",
		"email": "cathy@skytrac.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000024689",
		"Description": "Flybe Limited",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nichola Kingsury T-A Se7en",
	"Number": "CGB000156",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007862",
		"Description": "Nichola Kingsury T-A Se7en",
		"email": "nicholakingsbury@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Sheffield",
	"Number": "CUKCC0064",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149586",
		"Description": "John Lewis - Sheffield",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Glasgow",
	"Number": "CGB000612",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006826",
		"Description": "Alpha Flight Uk LTD - Tcx",
		"email": "scrowther@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "212 Glasgow",
	"Number": "CGB008953",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002987",
		"Description": "212 Glasgow",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB911 Customer",
	"Number": "SCAGB911",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005764",
		"Description": "JOHN LEWIS - WELWYN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Huddersfield",
	"Number": "CUKCC0026",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149548",
		"Description": "HOF - Huddersfield",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GLEN DORWARD",
	"Number": "CGB008013",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Brigdens LTD T-A Brigdens",
	"Number": "CGB000157",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007000",
		"Description": "Brigdens LTD T-A Brigdens",
		"email": "nigel@brigdens.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abi Finder",
	"Number": "CGB008933",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alisons LTD [Skegness] - Oscars",
	"Number": "CGB000040",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006856",
		"Description": "Alisons LTD [Skegness] - Oscars",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bethany Chard",
	"Number": "CGB002071",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Harmeet Gabrie",
	"Number": "CGB001768",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035152",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Chantal Brindley",
	"Number": "CGB010375",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dc Clothing LTD T-A D.Copperfield",
	"Number": "CGB000179",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007198",
		"Description": "Dc Clothing LTD T-A D.Copperfield",
		"email": "harryblock86@yahoo.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "vicky perry",
	"Number": "CGB008920",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB336 Customer",
	"Number": "SCAGB336",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005619",
		"Description": "HOUSE OF FRASER MAIDSTONE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "PYA Importer Ltd - The Bay",
	"Number": "CGB001328",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000023943",
		"Description": "PYA Importer Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Merepress LTD T-A Hurley",
	"Number": "CGB000168",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008035",
		"Description": "Merepress LTD T-A Hurley",
		"email": "mark@hurleys.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB922 Customer",
	"Number": "SCAGB922",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005780",
		"Description": "JOHN LEWIS - NORWICH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Magazine Zum Globus AG",
	"Number": "CGB000553",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007676",
		"Description": "Magazine Zum Globus AG",
		"email": "susann.ratai@globus.ch",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sandersons Department Stores Ltd",
	"Number": "CGB007033",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000038316",
		"Description": "Sandersons Department Stores Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Benita MISC Nwagwu",
	"Number": "CGB010652",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Krzysztof Zielosko",
	"Number": "CGB001573",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Soolmaz Karim",
	"Number": "CGB003621",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lance And Debbie Millett T-A",
	"Number": "CGB000966",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007714",
		"Description": "Lance And Debbie Millett T-A",
		"email": "debbie@brooksandleigh.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Albion 1879 - 338c Moraleja",
	"Number": "CGB000792",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006775",
		"Description": "Albion 1879 - 338c Moraleja",
		"email": "nigel@albionspain.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ross Hutchins",
	"Number": "CGB001023",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008305",
		"Description": "Ross Hutchins",
		"email": "ross_hutchins18@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dyhy",
	"Number": "CGB001131",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007274",
		"Description": "Dyhy",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JORDAN ALLEYNE",
	"Number": "CGB010161",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB620 Customer",
	"Number": "CGB010706",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "IBRAHIM ADEN",
	"Number": "CGB001883",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Chris Barrass",
	"Number": "CGB002133",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Denis Hope LTD",
	"Number": "CGB000087",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007226",
		"Description": "Denis Hope LTD",
		"email": "info@denishope.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Zeta Fashion LLC",
	"Number": "CGB005151",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033955",
		"Description": "Zeta Fashion LLC",
		"email": "khalig@zeta.az",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Town And Country Clitheroe LTD",
	"Number": "CGB000062",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007866",
		"Description": "Town And Country Clitheroe LTD",
		"email": "accounts@seasondesignerwear.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB321 Customer",
	"Number": "SCAGB321",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005625",
		"Description": "HOUSE OF FRASER OXFORD ST",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nordstrom West Covina Rack",
	"Number": "CGB008774",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039126",
		"Description": "0399 - S California DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Accessories Finland Oy",
	"Number": "CGB000174",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006789",
		"Description": "Accessories Finland Oy",
		"email": "marko@acc3ss.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg Retail Buying",
	"Number": "CGB001120",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008166",
		"Description": "Peek And  Cloppenburg Retail Buying",
		"email": "#log-import@peekundcloppenburg.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tom Morrow And Co LTD",
	"Number": "CGB000055",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008615",
		"Description": "Tom Morrow And Co LTD",
		"email": "shop@tommorrows.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jack Richards",
	"Number": "CGB002562",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Clare Collinson",
	"Number": "CGB010150",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "The Lingerie Boutique",
	"Number": "CGB000638",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007577",
		"Description": "The Lingerie Boutique",
		"email": "enquiries@the-lingerie-boutique.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "simon white",
	"Number": "CGB005359",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Maddy Parker CPS",
	"Number": "CGB003306",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Apple Emea Bulk",
	"Number": "CGB000695",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006605",
		"Description": "Apple Emea Bulk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aiste Kabasinskaite",
	"Number": "CGB001339",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Asos.Com Australia T-A Asos.Com LTD",
	"Number": "CGB001024",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006657",
		"Description": "Asos.Com Deliveries",
		"email": "asoscollection@wtprima.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MAX CONDIE",
	"Number": "CGB003242",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Wickham Properties Ltd t/a Noel's",
	"Number": "CGB008869",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044907",
		"Description": "Wickham Properties Ltd t/a Noel's",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "White Admiral LTD T-A Les Papillion",
	"Number": "CGB000742",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008146",
		"Description": "White Admiral LTD T-A Les Papillion",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Caramel Clothing LTD",
	"Number": "CGB000103",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007121",
		"Description": "Caramel Clothing LTD",
		"email": "sarah@caramelclothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "CHLOE CHEUNG",
	"Number": "CGB001674",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB267 Customer",
	"Number": "SCAGB267",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005601",
		"Description": "HEATHROW TERMINAL 4 - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Henrietta Mooney",
	"Number": "CGB003401",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Holby City Costume-Barbara Sweryda",
	"Number": "CGB001046",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007755",
		"Description": "Holby City Costume-Barbara Sweryda",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Phoebe Cross",
	"Number": "CGB003360",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "chanel smith",
	"Number": "CGB001566",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Cristina Garcia",
	"Number": "CGB007879",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mes Amis",
	"Number": "CGB000832",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007960",
		"Description": "Mes Amis",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "La Dunga Mode",
	"Number": "CGB001148",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007545",
		"Description": "La Dunga Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alex Pankhurst",
	"Number": "CGB009822",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Outstanding Branding",
	"Number": "CGB001309",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018818",
		"Description": "Citigroup Global Markets Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dapper [Scarboro] Ltd - Mainline MW",
	"Number": "CGB000044",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007177",
		"Description": "Dapper[Scarboro] Ltd t/a Mainline Mens",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Reading",
	"Number": "CUKCC0015",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149537",
		"Description": "HOF - Reading",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB339 Customer",
	"Number": "SCAGB339",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005622",
		"Description": "HOUSE OF FRASER MILTON KEYNES",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Aberdeen",
	"Number": "CUKCC0069",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149591",
		"Description": "John Lewis - Aberdeen",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Matt Molloy",
	"Number": "CGB002981",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "J E Beale PLC Accounts Dept",
	"Number": "CGB000013",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006725",
		"Description": "J E Beale PLC Accounts Dept",
		"email": "rferguson@beales.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "valentina Granatelli",
	"Number": "CGB001872",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Monty Smith LTD",
	"Number": "CGB000076",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008027",
		"Description": "Monty Smith LTD",
		"email": "woolnoughrai@sky.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sibirana Restano SL t/a Asterisco",
	"Number": "CGB007450",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041199",
		"Description": "Sibirana Restano SL t/a Asterisco",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB333 Customer",
	"Number": "SCAGB333",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005637",
		"Description": "HOUSE OF FRASER PLYMOUTH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Joseph And Co",
	"Number": "CGB000051",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007431",
		"Description": "Joseph And Co",
		"email": "josephandco@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Edinburgh Jenners",
	"Number": "CUKCC0036",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149558",
		"Description": "HOF - Edinburgh Jenners",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ste Alphy T-A New York",
	"Number": "CGB000936",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008096",
		"Description": "Ste Alphy T-A New York",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Special Partners SL t/a BTQ Kado",
	"Number": "CGB003512",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029196",
		"Description": "Special Partners S.L KADO",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Celia Sanchez t/a BTQ Zenia",
	"Number": "CGB008784",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044881",
		"Description": "Celia Sanchez t/a BTQ Zenia",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Karakter",
	"Number": "CGB001108",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007455",
		"Description": "Karakter",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Brand New",
	"Number": "CGB000843",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007004",
		"Description": "Brand New",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elle Coe",
	"Number": "CGB009286",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Agatha Clothing LTD",
	"Number": "CGB000060",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006850",
		"Description": "Agatha Clothing LTD",
		"email": "info@agatha-boutique.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "justyna lisowska",
	"Number": "CGB009507",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mrs J O'Brien Trading As Almond",
	"Number": "CGB000778",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006866",
		"Description": "Mrs J O'Brien Trading As Almond",
		"email": "totheobriens@aol.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Watford",
	"Number": "CUKCC0077",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149599",
		"Description": "John Lewis - Watford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB619 Customer",
	"Number": "CGB010611",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000146709",
		"Description": "GB619 Customer",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "STEVEN HANNIGAN",
	"Number": "CGB001746",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035157",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB371 Customer",
	"Number": "SCAGB371",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005646",
		"Description": "FENWICK BRENT CROSS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Vinny Phillips",
	"Number": "CGB005845",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Van Zuilen",
	"Number": "CGB000984",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008393",
		"Description": "Van Zuilen",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB237 Customer",
	"Number": "SCAGB237",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005565",
		"Description": "NOTTINGHAM - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kmi Brands",
	"Number": "CGB001212",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013050",
		"Description": "KMI BRANDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Juul",
	"Number": "CGB000027",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007447",
		"Description": "Juul",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "K Salim T-A Club Jj - Mens Store",
	"Number": "CGB000075",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007103",
		"Description": "K Salim T-A Club Jj - Mens Store",
		"email": "salimkidiya@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mared Williams",
	"Number": "CGB005206",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sam Mitchinson",
	"Number": "CGB003679",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwicks Ltd t./a Williams And Griffin",
	"Number": "CGB000152",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008451",
		"Description": "Williams And Griffin Deliveries",
		"email": "col.warehouse@wandg.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aaron Paul Payler",
	"Number": "CGB001569",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Clive Wilson",
	"Number": "CGB000927",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008449",
		"Description": "Clive Wilson",
		"email": "willo6@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "VIRTUAL CALGARY CHINOOK",
	"Number": "CGB008922",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000042451",
		"Description": "STCA008 - TED BAKER - CALGARY CHINOOK",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MINA BEGUM FAF",
	"Number": "CGB003375",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "THOMAS FORDER",
	"Number": "CGB010592",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lillie Barnsley",
	"Number": "CGB008678",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nordstrom Shoppers World Rack",
	"Number": "CGB009970",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039204",
		"Description": "0699 - Marlboro DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Katie Shipp",
	"Number": "CGB003284",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lauren Hill",
	"Number": "CGB003198",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ama Obiomah",
	"Number": "CGB003184",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Tin Chi",
	"Number": "CGB007921",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [Leic]",
	"Number": "CGB000163",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006896",
		"Description": "Fenwick Ltd [Leic]",
		"email": "jeremyharrington@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SARL COORDONNABLE",
	"Number": "CGB001689",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024647",
		"Description": "Coordinable",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB328 Customer",
	"Number": "SCAGB328",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005630",
		"Description": "HOUSE OF FRASER CITY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Karla Cutting",
	"Number": "CGB001801",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB353 Customer",
	"Number": "SCAGB353",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005644",
		"Description": "BENTALLS BRACKNELL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harper Roma SL t/a Cherie 1961",
	"Number": "CGB007622",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041679",
		"Description": "Harper Roma SL t/a Cherie 1961",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MISBEHAVIOUR SMS",
	"Number": "CGB000119",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007751",
		"Description": "MSBVR BV SMS",
		"email": "info@misbehaviour.nl",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sas Hkbs T-A Bubi Sheli",
	"Number": "CGB001067",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007028",
		"Description": "Sas Hkbs T-A Bubi Sheli",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lily T LTD",
	"Number": "CGB000194",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007581",
		"Description": "Lily T LTD",
		"email": "deborah@lily-t.com; andy@lily-t.com;andrewcollett@dsl.pipex.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ip Svetlichnaya Lv Ta Wo-Men",
	"Number": "CGB000951",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008459",
		"Description": "Ip Svetlichnaya Lv Ta Wo-Men",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis",
	"Number": "CGB000016",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007411",
		"Description": "John Lewis - Clipper Logistics",
		"email": "esutton@clippergroup.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007423",
		"Description": "John Lewis Magna Park",
		"email": "invoices@partnershipservices.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007427",
		"Description": "John Lewis Northampton",
		"email": "invoices@partnershipservices.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007429",
		"Description": "John Lewis Park Royal",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Reading",
	"Number": "CUKCC0075",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149597",
		"Description": "John Lewis - Reading",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Spriggs",
	"Number": "CGB004758",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Tom Hall",
	"Number": "CGB003675",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000032020",
		"Description": "Tom Hall Misc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Manchester",
	"Number": "CGB000601",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006802",
		"Description": "Alpha Flight Uk LTD - Bonded Stores",
		"email": "krenshaw@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bethan Ward Of Eccleshall",
	"Number": "CGB000649",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006737",
		"Description": "Bethan Ward Of Eccleshall",
		"email": "bethanwardofeccleshall@yahoo.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ego / SARL JDR",
	"Number": "CGB001320",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018832",
		"Description": "Ego / SARL JDR",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bssa The Hanger - Sms - Showroom",
	"Number": "CGB000699",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006916",
		"Description": "Bssa The Hanger - Sms - Showroom",
		"email": "lhollander@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Delscher",
	"Number": "CGB000962",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007214",
		"Description": "Delscher",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jennifer Cullen T A  Empress",
	"Number": "CGB000341",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007314",
		"Description": "Jennifer Cullen T A  Empress",
		"email": "info@empress.ie;  kcullen1980@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sean Spence FAF",
	"Number": "CGB004912",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045486",
		"Description": "Sean Spence FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB317 Customer",
	"Number": "SCAGB317",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005614",
		"Description": "HOUSE OF FRASER LEAMINGTON SPA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Boros LTD - Crawley",
	"Number": "CGB000106",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006971",
		"Description": "Boros LTD - Crawley",
		"email": "borosleathergoods@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - High Wycombe",
	"Number": "CUKCC0022",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149544",
		"Description": "HOF - High Wycombe",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lucy Deakin",
	"Number": "CGB002393",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Lsl Leeds Bradford",
	"Number": "CGB000916",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007639",
		"Description": "Alpha Lsg LTD - Lsl Leeds Bradford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB256 Customer",
	"Number": "SCAGB256",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005588",
		"Description": "TRAFFORD CENTRE - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Boutique Michelle",
	"Number": "CGB001060",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007968",
		"Description": "Boutique Michelle",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Chloe Neave",
	"Number": "CGB001350",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Trafford",
	"Number": "CUKCC0072",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149594",
		"Description": "John Lewis - Trafford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB306 Customer",
	"Number": "SCAGB306",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005606",
		"Description": "SELFRIDGES BIRMINGHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - Canterbury",
	"Number": "CUKCC0047",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149569",
		"Description": "Fenwicks - Canterbury",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Vente-Privee.com",
	"Number": "CGB007621",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041672",
		"Description": "Vente-Privee.com SMS Deliveries",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "PYA Importer Ltd",
	"Number": "CGB000178",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007054",
		"Description": "PYA Importer Ltd SMS",
		"email": "sambas@pyaimporter.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Janella Hayes DO NOT USE",
	"Number": "CGB008301",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Galleries [Fashions] LTD",
	"Number": "CGB000098",
	"Group": "NTH",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "Galleries [Fashions] LTD T-A Thackerays",
		"Description": "W-INDIE",
		"email": "000008579",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[Galleries [Fashions] LTD T-A Thackerays] is greater than 15 characters)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB263 Customer",
	"Number": "SCAGB263",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005596",
		"Description": "HEATHROW TERMINAL 2 - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Hut.Com LTD",
	"Number": "CGB000326",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007793",
		"Description": "The Hut Group - OMEGA",
		"email": "apparel.goodsin@thehutgroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Seiko Uk LTD Fao Sue Nicol",
	"Number": "CGB001029",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007874",
		"Description": "Seiko Uk LTD Fao Sue Nicol",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selina Durrani",
	"Number": "CGB008550",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Esther Carrion - F&F - 40",
	"Number": "CGB009863",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000073876",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bastian Troelsen",
	"Number": "CGB009181",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Blijdesteijn Mode",
	"Number": "CGB000821",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006749",
		"Description": "Blijdesteijn Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB248 Customer",
	"Number": "SCAGB248",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005579",
		"Description": "HEATHROW TERMINAL 5 - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nineteen Seventy Eight LTD",
	"Number": "CGB000746",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008092",
		"Description": "Nineteen Seventy Eight LTD",
		"email": "info@threadsmenswear.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tjx Uk Specials",
	"Number": "CGB001270",
	"Group": "W-OFFPRICE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008611",
		"Description": "Tjx Uk Specials",
		"email": "accountspayable_invoices@tjxeurope.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Edingburgh",
	"Number": "CUKCC0058",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149580",
		"Description": "John Lewis - Edingburgh",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Teds Grooming Room Fao Demet Kaya",
	"Number": "CGB001276",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007294",
		"Description": "Teds Grooming Room Fao Demet Kaya",
		"email": "mus@tedsgroomingroom.com; georgina@tedsgroomingroom.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MARK KIDGELL",
	"Number": "CGB002977",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB230_Customer",
	"Number": "SCAGB230",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005561",
		"Description": "FLORAL STREET - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Red E2 Ltd",
	"Number": "CGB001745",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024783",
		"Description": "Red E2 Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB269",
	"Number": "CGB001522",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000145084",
		"Description": "SPITALFIELDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charlotte Ile",
	"Number": "CGB001424",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Seonaid Murphy",
	"Number": "CGB003703",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031488",
		"Description": "Seonaid Murphy CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Katrina  Lamb T A Coastal Culture",
	"Number": "CGB000308",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007065",
		"Description": "Katrina  Lamb T A Coastal Culture",
		"email": "katrina@candy-coast.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "D409 SL",
	"Number": "CGB003443",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029191",
		"Description": "D409 SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Erin Boreham",
	"Number": "CGB010066",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB001104",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006888",
		"Description": "Ansons - Hamburg Dc",
		"email": "#log-import@ansons.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Phillip Blakeman",
	"Number": "CGB001901",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037779",
		"Description": "DO NOT USE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB241 Customer",
	"Number": "SCAGB241",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005569",
		"Description": "CHESHIRE OAKS - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB617 Customer",
	"Number": "SCAGB617",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045982",
		"Description": "STGB617 - TEDCOMM - IRELAND",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Megan Thomas",
	"Number": "CGB006026",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Michael Nobbs",
	"Number": "CGB008223",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Caprice Chelsea Grill Ltd",
	"Number": "CGB010176",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000111845",
		"Description": "Caprice Chelsea Grill Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "adam cook CPS",
	"Number": "CGB008285",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000148522",
		"Description": "adam cook cps",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Classics Lifestyle bv",
	"Number": "CGB001317",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018820",
		"Description": "Classics Lifestyle bv",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Luiza Slawaniska",
	"Number": "CGB003407",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jirabi Nicodemus",
	"Number": "CGB002599",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Tinashe Mpavaenda",
	"Number": "CGB001634",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Demsa - Wholesale",
	"Number": "CGB000191",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007218",
		"Description": "Demsa Ic Ve Dis Ticaret As",
		"email": "demircanerdogan@demsagroup.com; burakturan@demsagroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Veena Co Ltd t/a Btq Walk",
	"Number": "CGB006083",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036156",
		"Description": "Veena Co LTD t/a BTQ Walk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Marius Caldararu",
	"Number": "CGB001377",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sam Brown",
	"Number": "CGB002932",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lexi Cohen",
	"Number": "CGB009692",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "ALEX GRAY",
	"Number": "CGB001737",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024777",
		"Description": "ALEX GRAY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rhianna Claridge",
	"Number": "CGB004736",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Kat Crawford",
	"Number": "CGB002082",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030615",
		"Description": "Kat Crawford FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peerin bv",
	"Number": "CGB001310",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018824",
		"Description": "Peerin bv",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nouvo Wilco Fashion",
	"Number": "CGB001147",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008082",
		"Description": "Nouvo Wilco Fashion",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Blueberries Blackpool LTD",
	"Number": "CGB000038",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006963",
		"Description": "Blueberries Blackpool LTD",
		"email": "sarah@blueberries-online.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J And S Turner LTD T-A The Dresser",
	"Number": "CGB001271",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008561",
		"Description": "J And S Turner LTD T-A The Dresser",
		"email": "james@the-dresser.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kristian Seaborn",
	"Number": "CGB009138",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003435",
		"Description": "Kristian Seaborn",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALISON HEWIT",
	"Number": "CGB001418",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB375 Customer",
	"Number": "SCAGB375",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005648",
		"Description": "FENWICK TUNBRIDGE WELLS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Repertoire Watford",
	"Number": "CGB000115",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008353",
		"Description": "Repertoire Watford",
		"email": "beverleygough@btinternet.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Luke Budgen CPS",
	"Number": "CGB001623",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041812",
		"Description": "luke budgen cps",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ouafa El Aomari",
	"Number": "CGB005950",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dean Cooper Cunningham",
	"Number": "CGB008829",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Very Exclusive - Shop Direct Home Shopping Ltd",
	"Number": "CGB008268",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043560",
		"Description": "Very Exclusive - Delivery",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Endless Gabbata SL",
	"Number": "CGB005838",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034412",
		"Description": "Endless Gabbata SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD  - Ls2 Manchester",
	"Number": "CGB000250",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007621",
		"Description": "Lsg Sky Chefs Uk LTD - Manchester",
		"email": "laura.razmaite@lsgskychefs.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg Retail Buying",
	"Number": "CGB001107",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008172",
		"Description": "Peek And Cloppenburg Kg Store",
		"email": "#log-import@peekundcloppenburg.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALAN PIKE",
	"Number": "CGB001817",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB009645",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000069813",
		"Description": "Peek & Cloppenburg Vienna",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jodie Chard",
	"Number": "CGB001864",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nicholas Lacson",
	"Number": "CGB001587",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarl Jale T-A Joya",
	"Number": "CGB000988",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007435",
		"Description": "Sarl Jale T-A Joya",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lee Cain",
	"Number": "CGB002010",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Paul Tisdale Exeter Afc LTD",
	"Number": "CGB000940",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008623",
		"Description": "Paul Tisdale Exeter Afc LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Louis Copeland And Sons [Pembroke",
	"Number": "CGB001113",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007533",
		"Description": "Louis Copeland And Sons [Pembroke",
		"email": "adrian@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Exeter",
	"Number": "CUKCC0021",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149543",
		"Description": "HOF - Exeter",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carmen Penuela Borja t/a Borja",
	"Number": "CGB007445",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041178",
		"Description": "Carmen Penuela Borja t/a Borja",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarl Drapier T-A Dewachter",
	"Number": "CGB000879",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007234",
		"Description": "Sarl Drapier t/a Dewachter",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Maidstone",
	"Number": "CUKCC0031",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149553",
		"Description": "HOF - Maidstone",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carolina Malko",
	"Number": "CGB008256",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lyndonmont LTD T-A Dylan Hotel",
	"Number": "CGB001118",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007278",
		"Description": "Lyndonmont LTD T-A Dylan Hotel",
		"email": "barryfrancis@dylan.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Deelux Trading AG",
	"Number": "CGB006480",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037158",
		"Description": "Deelux Trading AG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Xhordie Lleshi",
	"Number": "CGB004066",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031562",
		"Description": "Xhordi CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "BRINK & CAMPMAN B.V",
	"Number": "CGB007319",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000040188",
		"Description": "BRINK & CAMPMAN B.V",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB902 Customer",
	"Number": "SCAGB902",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005756",
		"Description": "JOHN LEWIS - OXFORD ST",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Croydon",
	"Number": "CUKCC0042",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149564",
		"Description": "HOF - Croydon",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "TOM WARD",
	"Number": "CGB009152",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ruby Ahmed",
	"Number": "CGB002955",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ws Pettit And Co LTD [Mex]",
	"Number": "CGB000094",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008236",
		"Description": "Ws Pettit And Co LTD [Mex]",
		"email": "paul@pettits.com; fiona@pettits.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jackie Sin",
	"Number": "CGB008187",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Brief Encounter",
	"Number": "CGB000184",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006996",
		"Description": "Brief Encounter",
		"email": "burgess-c2@sky.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ABDI AHMED MUSE",
	"Number": "CGB009860",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB268 Customer",
	"Number": "SCAGB268",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005602",
		"Description": "BIRMINGHAM BULLRING - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Paris Clothing LTD T-A Paris",
	"Number": "CGB000102",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008224",
		"Description": "Paris Clothing LTD T-A Paris",
		"email": "info@paris-clothing.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000008226",
		"Description": "Paris Clothing LTD T-A Paris",
		"email": "info@paris-clothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB259 Customer",
	"Number": "SCAGB259",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005591",
		"Description": "GLASGOW BUCHANAN ST - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elisha Vanduesburg-Smith",
	"Number": "CGB010586",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lucia Russo",
	"Number": "CGB002176",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Richmond Classics Limited",
	"Number": "CGB000141",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008283",
		"Description": "Richmond Classics Limited",
		"email": "accounts@richmondclassics.net",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Racheal Spink - Do Not Use",
	"Number": "CGB003241",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [York]",
	"Number": "CGB000139",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006933",
		"Description": "Fenwicks LTD [York]",
		"email": "yorkreception@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "RTWPerformanceTest",
	"Number": "CGB010732",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000150251",
		"Description": "RTWPerformanceTest",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Samina Mughal",
	"Number": "CGB001727",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Glassoracle LTD T-A Equus",
	"Number": "CGB000126",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007097",
		"Description": "Glassoracle Ltd t/a Equus",
		"email": "lina.walker@virgin.net",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007147",
		"Description": "Glassoracle LTD T-A Equus",
		"email": "lina.walker@virgin.net",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tjx Uk",
	"Number": "CGB000012",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008607",
		"Description": "Tjx Uk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Psycle Ltd t/a Psycle London C.Wharf",
	"Number": "CGB006244",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036578",
		"Description": "Psycle London",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And Cloppenburg Retail Buying GmbH & Co. KG",
	"Number": "CGB001179",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008170",
		"Description": "Peek And Cloppenburg Ax Inv Code",
		"email": "#log-import@peekundcloppenburg.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Raj LTD",
	"Number": "CGB000022",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008269",
		"Description": "Raj LTD",
		"email": "raju@marblearc.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Westmond Marktamara Belgium Bvba",
	"Number": "CGB000865",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008417",
		"Description": "Westmond Marktamara Belgium Bvba",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Cardiff",
	"Number": "CGB000604",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006814",
		"Description": "Alpha Flight Uk LTD - Tcx",
		"email": "mricketts@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LTA Operations Ltd",
	"Number": "CGB000920",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007647",
		"Description": "LTA Operations Ltd",
		"email": "rebecca.james@lta.org.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Vollebregt Mode Bv",
	"Number": "CGB001142",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008367",
		"Description": "Vollebregt Mode Bv",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bethany Walker",
	"Number": "CGB008296",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Apple Japan Inc",
	"Number": "CGB000685",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006615",
		"Description": "Apple Japan Inc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarah Scanlan",
	"Number": "CGB001771",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Iris",
	"Number": "CGB000033",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008126",
		"Description": "Iris",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J E Beale PLC 08 Yeovil - Denners",
	"Number": "CGB000131",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006705",
		"Description": "J E Beale PLC 08 Yeovil - Denners",
		"email": "hpeyman@beales.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wp Brown LTD T-A Browns York",
	"Number": "CGB000243",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008463",
		"Description": "Browns Warehouse",
		"email": "info@browns-york.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Van Tilburg",
	"Number": "CGB000958",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008387",
		"Description": "Van Tilburg",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Stockmann Baltics",
	"Number": "CGB000195",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008521",
		"Description": "Stockmann Baltics",
		"email": "jari@acc3ss.com; samu@acc3ss.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nicola Bradshaw",
	"Number": "CGB001345",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Troia Restaurants UK Ltd t/a The Ivy",
	"Number": "CGB008706",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044675",
		"Description": "The Ivy Cafe Wimbledon Village",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000044676",
		"Description": "The Ivy Clifton Brasserie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000044813",
		"Description": "The Ivy Kensington Brasserie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mr Gees LTD",
	"Number": "CGB000049",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006952",
		"Description": "Mr Gees LTD",
		"email": "gees09@live.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Norwich",
	"Number": "CUKCC0076",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149598",
		"Description": "John Lewis - Norwich",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selin Yasar",
	"Number": "CGB005181",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Image Wear",
	"Number": "CGB001137",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007808",
		"Description": "Image Wear",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Linda Tait t/a Sunflowers Lingerie",
	"Number": "CGB003179",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024794",
		"Description": "Sunflowers",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gavin Slough",
	"Number": "CGB002056",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "238 Bridgend",
	"Number": "CGB007849",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002968",
		"Description": "238 Bridgend",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nordstrom Fashion Valley",
	"Number": "CGB008249",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000039127",
		"Description": "0399 - S California DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Matt La Moda LTD T-A",
	"Number": "CGB000763",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007994",
		"Description": "Matt La Moda LTD T-A",
		"email": "info@mattlamoda.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sa Loisirs Soleil T-A Boutique Club",
	"Number": "CGB001018",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007107",
		"Description": "Sa Loisirs Soleil T-A Boutique Club",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Natalie Roche",
	"Number": "CGB003423",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Furniss Lindsay Ta Pollard And Read",
	"Number": "CGB000637",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008208",
		"Description": "Furniss Lindsay Ta Pollard And Read",
		"email": "info@pollardandread.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Marija Laurinaviciute",
	"Number": "CGB001774",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Hazel Simpson",
	"Number": "CGB003826",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Moss Bros Royalty",
	"Number": "CGB001185",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013002",
		"Description": "MOSS BROS ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Db3 [Boutiques] LTD",
	"Number": "CGB001091",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007194",
		"Description": "Db3 [Boutiques] LTD",
		"email": "sales@db3online.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarl Stacia Ketoff T-A Ketoff",
	"Number": "CGB000862",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007465",
		"Description": "Sarl Stacia Ketoff T-A Ketoff",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Brent Cross",
	"Number": "CUKCC0060",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149582",
		"Description": "John Lewis - Brent Cross",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Austine Humphrey",
	"Number": "CGB003336",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mitch Hunter",
	"Number": "CGB001367",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ollyhurst LTD T-A Union CLOSED",
	"Number": "CGB001124",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008118",
		"Description": "Ollyhurst LTD T-A Union",
		"email": "richard@union-clothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Piet Zoomers",
	"Number": "CGB001115",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008250",
		"Description": "Piet Zoomers",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB245 Customer",
	"Number": "SCAGB245",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005573",
		"Description": "BRIGHTON - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tura Lp",
	"Number": "CGB001197",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013016",
		"Description": "TURA LP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Cheltenham",
	"Number": "CUKCC0029",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149551",
		"Description": "HOF - Cheltenham",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB613 Customer",
	"Number": "SCAGB613",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045977",
		"Description": "STGB613 - TEDCOMM - SOUTH EU",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harrods",
	"Number": "CUKCC0053",
	"Group": "R-CON-HAR",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149575",
		"Description": "Harrods",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Birmingham",
	"Number": "CUKCC0087",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149379",
		"Description": "STGB - JOHN LEWIS - BIRMINGHAM",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB916 Customer",
	"Number": "SCAGB916",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005773",
		"Description": "JOHN LEWIS - GLASGOW",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Choice Clothes Leon SL",
	"Number": "CGB003206",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026528",
		"Description": "Choice Clothes Leon S.L",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Killow Retail Galway Ltd t-a Willow",
	"Number": "CGB001806",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000023959",
		"Description": "Willow Galway",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Michelle Folland",
	"Number": "CGB001414",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Becky Dunn",
	"Number": "CGB001614",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Liam Bowen",
	"Number": "CGB003616",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Coneys Of Boston LTD",
	"Number": "CGB000189",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007125",
		"Description": "Coneys Of Boston LTD T-A Coneys Of Boston",
		"email": "coneys1@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harrison Degannes",
	"Number": "CGB001733",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Simion Turcan",
	"Number": "CGB010019",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "JONNY HEALY",
	"Number": "CGB008673",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jack Couch",
	"Number": "CGB003270",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Studio 10 - Sms",
	"Number": "CGB000146",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006721",
		"Description": "Studio 10 - Sms",
		"email": "kerensa@lincinternational.be",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Barkers of Northallerton Ltd",
	"Number": "CGB007443",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041070",
		"Description": "Barkers of Northallerton Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "demi a abbott",
	"Number": "CGB007907",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Amanda Scott",
	"Number": "CGB010168",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "CHRIS WILSON",
	"Number": "CGB006171",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Stijl Trend Bergen",
	"Number": "CGB000944",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008513",
		"Description": "Stijl Trend Bergen",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Joshua Guerrero",
	"Number": "CGB001637",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB326 Customer",
	"Number": "SCAGB326",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005632",
		"Description": "HOUSE OF FRASER HIGH WYCOMBE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amelia Brown",
	"Number": "CGB001529",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Emily Ellick",
	"Number": "CGB007943",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Demsa Ic Ve Dis Ticaret A.S",
	"Number": "CGB001249",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013100",
		"Description": "DEMSA IC VE DIS TICARET A.S",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Houtbrox Deliveries",
	"Number": "CGB000886",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007761",
		"Description": "Houtbrox Deliveries",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "House Of Fraser",
	"Number": "CGB000001",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007749",
		"Description": "National Distribution Centre",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB340 Customer",
	"Number": "SCAGB340",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005639",
		"Description": "HOUSE OF FRASER LAKESIDE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Van Der Kam Zutphen Womens",
	"Number": "CGB001157",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008679",
		"Description": "Van Der Kam Womenswear",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Marion Dumontet",
	"Number": "CGB007820",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044369",
		"Description": "Marion Dumontet CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ted Baker Downunder Pty. LTD",
	"Number": "CGB000120",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006681",
		"Description": "Ted Baker Downunder Pty. LTD",
		"email": "laki.fernando@tedbaker.com.au",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "JORDAN BIZZELL",
	"Number": "CGB002667",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Christian Baskerville MISC",
	"Number": "CGB009383",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Tara Johal",
	"Number": "CGB005570",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jake Reed",
	"Number": "CGB002464",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "HARRODS - EDI Acc",
	"Number": "CUKHA1000",
	"Group": "R-CON-HAR",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149888",
		"Description": "HARRODS - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mirror Store SL t/a Santa Marta",
	"Number": "CGB007449",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041195",
		"Description": "Mirror Store SL t/a Santa Marta",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Italy - Flextronics Global",
	"Number": "CGB000691",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006613",
		"Description": "Apple Italy - Flextronics Global",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Oscars [Grimsby] LTD",
	"Number": "CGB000061",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008134",
		"Description": "Oscars [Grimsby] LTD",
		"email": "capitalcollection@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "DSTGB607 Customer",
	"Number": "CGB010699",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB927 Customer",
	"Number": "SCAGB927",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005786",
		"Description": "JOHN LEWIS - CANARY WHARF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Evija Macala",
	"Number": "CGB008252",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Armaan M Sdn Bhd - RSH Malaysia",
	"Number": "CGB001243",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013090",
		"Description": "ARMAAN M SDN BHD - RSH MALAYSIA SDN",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB227 Customer",
	"Number": "SCAGB227",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005559",
		"Description": "SHEFFIELD - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bianca Heath",
	"Number": "CGB010034",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Catherine Ruth Gatenby",
	"Number": "CGB001366",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mel Rodrigues - AL",
	"Number": "CGB001659",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005330",
		"Description": "Mel Rodrigues - AL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dolores Copeland",
	"Number": "CGB003176",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Danielle Short",
	"Number": "CGB007670",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Marios Ento-Enkolo",
	"Number": "CGB007704",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Annette Mode",
	"Number": "CGB000781",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006581",
		"Description": "Annette Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carmen Suarez SL",
	"Number": "CGB003314",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027731",
		"Description": "Carmen Suarez S.L",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J E Beale 01 Bournemouth",
	"Number": "CGB000135",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006697",
		"Description": "J E Beale 01 Bournemouth",
		"email": "hpeyman@beales.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Redfords",
	"Number": "CGB000003",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008291",
		"Description": "Redfords",
		"email": "kevin@redfordsmenswear.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Vivat",
	"Number": "CGB001129",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008663",
		"Description": "Vivat",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dale Coleridge - cps",
	"Number": "CGB010374",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000132721",
		"Description": "Dale Coleridge - CPS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000132726",
		"Description": ".",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Darlington",
	"Number": "CUKCC0018",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149540",
		"Description": "HOF - Darlington",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "De Nobelaer",
	"Number": "CGB001135",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007222",
		"Description": "De Nobelaer",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sebbi",
	"Number": "CGB000871",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007870",
		"Description": "Sebbi",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Louise Collins - 75",
	"Number": "CGB003568",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003074",
		"Description": "Louise Collins",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jazznote LTD T-A Zucci",
	"Number": "CGB000066",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008491",
		"Description": "Jazznote LTD T-A Zucci",
		"email": "zucci.clothes@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jarrold And Sons LTD",
	"Number": "CGB000083",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007836",
		"Description": "Jarrold And Sons LTD",
		"email": "plinvoicing@jarrold.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lisa Wilkins",
	"Number": "CGB001800",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000003516",
		"Description": "Lisa Wilkins",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "julia chaban",
	"Number": "CGB009994",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Rjc Fashion LTD T-A Bella Sola",
	"Number": "CGB000269",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007026",
		"Description": "Rjc Fashion LTD T-A Bella Sola",
		"email": "style@bellasola.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sisera",
	"Number": "CGB001146",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007900",
		"Description": "Sisera",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Voorwinden",
	"Number": "CGB001323",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018857",
		"Description": "Voorwinden",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mary Jane Kerman",
	"Number": "CGB006105",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Megan Butler",
	"Number": "CGB009049",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Harrods Dept 113 Vendor No 15797703",
	"Number": "CGB000107",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007710",
		"Description": "Harrods-Thatcham Dc Dep 113",
		"email": "harrodstvdcbookings@harrods.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amanda Bates Ta Sunflowers Lingerie",
	"Number": "CGB000994",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008535",
		"Description": "Amanda Bates Ta Sunflowers Lingerie1234567890123456789",
		"email": "sunflow.bates@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Turnstyle Menswear LTD",
	"Number": "CGB000090",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007553",
		"Description": "Turnstyle Menswear LTD T/A Lemon Tree",
		"email": "jacky 07802558993",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Uniek Mode",
	"Number": "CGB001312",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018828",
		"Description": "Uniek Mode",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Arpan Gurung",
	"Number": "CGB008781",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB930 Customer",
	"Number": "SCAGB930",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005789",
		"Description": "JOHN LEWIS - STRATFORD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Abigail Wong",
	"Number": "CGB009887",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lincoln Bra Lady Ltd",
	"Number": "CGB003490",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027766",
		"Description": "Lincoln Bra Lady",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Anamaria Bursuc",
	"Number": "CGB010246",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Graham Menswear",
	"Number": "CGB000088",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007692",
		"Description": "Graham Menswear",
		"email": "info@grahamsmenswear.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Beautiful Spots LTD T-A Teds",
	"Number": "CGB000683",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008557",
		"Description": "Beautiful Spots LTD T-A Teds",
		"email": "pallypagliuca@benitobrowbar.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Farai Maganga",
	"Number": "CGB003812",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Grupo Optico Novalux t/a Tabora",
	"Number": "CGB008785",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044889",
		"Description": "Grupo Optico Novalux T/A Tabora",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Flore 39 Agencies Ltd - Ian",
	"Number": "CGB000173",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007805",
		"Description": "Flore 39 Agencies Ltd - Ian Home",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007806",
		"Description": "Flore 39 Agencies Ltd - Ian UBB",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Play Paddock",
	"Number": "CGB008802",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044877",
		"Description": "Play Paddock S.L T/A Boutique Play",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jennifer McDonald",
	"Number": "CGB009290",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Daniel Brolls",
	"Number": "CGB010615",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000147252",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "HOF - Bluewater",
	"Number": "CUKCC0040",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149562",
		"Description": "HOF - Bluewater",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carmen Olivari",
	"Number": "CGB001730",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Galleries [Fashions] LTD - Invoices",
	"Number": "CGB000151",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006942",
		"Description": "Galleries [Fashions] LTD - Invoices",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Leanne Soane",
	"Number": "CGB001530",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Yunny Wan",
	"Number": "CGB009983",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Noel Mulroy",
	"Number": "CGB001790",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB249 Customer",
	"Number": "SCAGB249",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005580",
		"Description": "CHEAPSIDE - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Invoices",
	"Number": "CGB000853",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006870",
		"Description": "Alpha Lsg LTD - Invoices",
		"email": "pledger_flight@alpha-group.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elaine Houghton t/a The Corner Shop",
	"Number": "CGB003305",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026530",
		"Description": "Elaine Houghton t/a The Corner Shop",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Danyl-James Turvey",
	"Number": "CGB008260",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "E-Trend T-A Monshowroom.Com",
	"Number": "CGB000887",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008043",
		"Description": "E Trend Chez Morin Logistic",
		"email": "severine@monshowroom.com; compta@monshowroom.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MICHAEL BLACK",
	"Number": "CGB008010",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Georgina Spearpoint",
	"Number": "CGB003378",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000032683",
		"Description": "georgie spearpoint",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Leigh-Ann Bovis",
	"Number": "CGB003234",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Renes",
	"Number": "CGB000626",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008295",
		"Description": "Renes",
		"email": "renes@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks York",
	"Number": "CUKCC0050",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149377",
		"Description": "STGB - FENWICKS YORK",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB238 Customer",
	"Number": "SCAGB238",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005566",
		"Description": "BRIDGEND - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ryan Handley",
	"Number": "CGB010167",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Maronmarch LTD",
	"Number": "CGB000166",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007926",
		"Description": "Maronmarch LTD",
		"email": "kelvin@maronmarch.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "otis dickinson",
	"Number": "CGB010606",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sophie Bray",
	"Number": "CGB002638",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Ls3 Lgw Strategic",
	"Number": "CGB000750",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007617",
		"Description": "Alpha Lsg LTD - Gatwick",
		"email": "daniel.szynalik@alphalsg.co.uk; marcin.zwada@alphalsg.co.uk;  ingrida.zitkute@alphalsg.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB337 Customer",
	"Number": "SCAGB337",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005620",
		"Description": "HOUSE OF FRASER BELFAST",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Cure Midland LTD T-A Cure",
	"Number": "CGB000712",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007171",
		"Description": "Cure Midland LTD T-A Cure",
		"email": "anna@cureuk.com; bev@cureuk.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "syriah Nicol Ntim",
	"Number": "CGB003425",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Alex Dimitrova",
	"Number": "CGB001565",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "henry wheeler",
	"Number": "CGB009792",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
	"Number": "CGB007456",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041213",
		"Description": "Peek & Cloppenburg Vienna Buying Gmbh & Co KG",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Armaan (Thailand) Co LTD Royalty",
	"Number": "CGB001204",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013032",
		"Description": "ARMAAN THAILAND CO LTD ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Erblina Kacani",
	"Number": "CGB003223",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nigel Jameson T-A Ivan Jameson",
	"Number": "CGB000072",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007818",
		"Description": "Nigel Jameson T-A Ivan Jameson",
		"email": "office@ivanjameson.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "S Bishop & Sons Ltd t/a Lily & Grace Lingerie",
	"Number": "CGB007034",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034773",
		"Description": "S Bishop & Sons Ltd t/a Lily & Grace Lingerie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Carmay Ltd t/a Itso Me",
	"Number": "CGB003859",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029172",
		"Description": "Carmay Ltd t/a Itso Me",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Knetter",
	"Number": "CGB000961",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007489",
		"Description": "Knetter",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Whitney Mensah",
	"Number": "CGB001368",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Comercial Macys Canarias S.L",
	"Number": "CGB001321",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018814",
		"Description": "Comercial Macys Canarias S.L",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges- Trafford Centre",
	"Number": "CUKCC0002",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149524",
		"Description": "Selfridges- Trafford Centre",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lyne Auger",
	"Number": "CGB001170",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006677",
		"Description": "Lyne Auger",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dresscode",
	"Number": "CGB001130",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007258",
		"Description": "Dresscode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kru of Eccleshall Ltd",
	"Number": "CGB000144",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007497",
		"Description": "Kru of Eccleshall Ltd",
		"email": "krug@veuveofstafford.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rav Dhillon",
	"Number": "CGB004938",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "chris simon",
	"Number": "CGB006113",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Kristian Brown",
	"Number": "CGB009506",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Louby-Lou Lingerie Ltd",
	"Number": "CGB003623",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000028385",
		"Description": "louby-Lou Lingerie LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charlee Wright",
	"Number": "CGB002993",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mark Smith",
	"Number": "CGB009538",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000000121",
		"Description": "Mark Smith",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bradley Knipe LTD",
	"Number": "CGB000864",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006745",
		"Description": "Bradley Knipe LTD",
		"email": "carl@bradleyknipe.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LOLA PERTE",
	"Number": "CGB002136",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Podium Magnitogorsk",
	"Number": "CGB001134",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008181",
		"Description": "Podium Magnitogorsk",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB251 Customer",
	"Number": "SCAGB251",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005582",
		"Description": "SOUTH MOLTON STREET - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MISBEHAVIOUR Office",
	"Number": "CGB001030",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007980",
		"Description": "MSBVR BV Stock Deliveries",
		"email": "info@misbehaviour.nl",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The House",
	"Number": "CGB000054",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007773",
		"Description": "The House",
		"email": "juliet@thehouseyarm.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Voortman",
	"Number": "CGB000839",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008371",
		"Description": "Voortman",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mariam Elmahdey",
	"Number": "CGB001576",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Connor Ralls",
	"Number": "CGB007716",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Calico Cornwall LTD T-A Calico",
	"Number": "CGB000158",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007044",
		"Description": "Calico Cornwall LTD T-A Calico",
		"email": "calico39a@aol.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sam Dennis",
	"Number": "CGB003251",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Natalie Manfield",
	"Number": "CGB008507",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Charles Ruby",
	"Number": "CGB003201",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "CB Marros t/a Btq Vertigo",
	"Number": "CGB005840",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034279",
		"Description": "CB Marros t/a Btq Vertigo",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lapel Menswear Ltd",
	"Number": "CGB008241",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043328",
		"Description": "Lapel Menswear",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fifty Six",
	"Number": "CGB000825",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007381",
		"Description": "Fifty Six",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Noreng Bawn T-A Chris Curtis",
	"Number": "CGB001109",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007521",
		"Description": "Noreng Bawn T-A Chris Curtis",
		"email": "wayne@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Matt Horner",
	"Number": "CGB007981",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sophie Seickell CPS",
	"Number": "CGB002853",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030853",
		"Description": "Sophie",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Levenslust",
	"Number": "CGB000950",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007569",
		"Description": "Levenslust",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Netherlands-Scandinavia",
	"Number": "CGB000690",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006619",
		"Description": "Apple Netherlands-Scandinavia",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Welwyn",
	"Number": "CUKCC0065",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149587",
		"Description": "John Lewis - Welwyn",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg Retail Buying",
	"Number": "CGB001033",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006880",
		"Description": "Ansons - Langenfeld Dc",
		"email": "#log-import@ansons.de;matthias.guese@ansons.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Neiman Marcus Beverly Hills",
	"Number": "CGB010258",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046442",
		"Description": "Neiman Marcus 091-NSC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046508",
		"Description": "Neiman Marcus Beverly Hills",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Tjx Uk Invoices",
	"Number": "CGB000155",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008601",
		"Description": "Tjx Uk Invoices",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Vikki Nixon",
	"Number": "CGB001838",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031463",
		"Description": "Vikki Nixon misc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ali Hann",
	"Number": "CGB008531",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Beata Baranowska",
	"Number": "CGB003577",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dabsan International Sa",
	"Number": "CGB001264",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013122",
		"Description": "DABSAN INTERNATIONAL SA",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charley Jones - AL",
	"Number": "CGB003676",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030599",
		"Description": "Charley Jones - AL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Maria Felix-Young",
	"Number": "CGB003385",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Dfass Uk LTD C-O Gate Gourmet",
	"Number": "CGB001174",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008655",
		"Description": "Dfass Uk LTD C-O Gate Gourmet",
		"email": "anthony.fletorides@dfassgroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Downtown Superstore - Oldrids",
	"Number": "CGB000190",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008110",
		"Description": "Downtown Superstore - Oldrids",
		"email": "tina.askew@oldrids.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Emma Marsicano",
	"Number": "CGB007749",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarl Jap t/a Magazine",
	"Number": "CGB001149",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007662",
		"Description": "Sarl Jap t/a Magazine",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOUSE OF FRASER - EDI Acc",
	"Number": "CUKHO1000",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149889",
		"Description": "HOUSE OF FRASER - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB904 Customer",
	"Number": "SCAGB904",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005758",
		"Description": "JOHN LEWIS - EDINBURGH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kieran McCaughey",
	"Number": "CGB004774",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033389",
		"Description": "Kieran McCaughey FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Neiman Marcus Topanga",
	"Number": "CGB009906",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000046473",
		"Description": "Neiman Marcus 091-NSC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000046537",
		"Description": "Neiman Marcus Topanga",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB614 Customer",
	"Number": "SCAGB614",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045979",
		"Description": "STGB614 - TEDCOMM - FRANCE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hampton Retail LTD T-A Banana",
	"Number": "CGB000779",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007704",
		"Description": "Hampton Retail LTD T-A Banana",
		"email": "sanj77@homail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Canedo Azcona t/a Btq Brizna",
	"Number": "CGB005844",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034052",
		"Description": "Micaela Azona t/a Btq Brizna",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB205 Customer",
	"Number": "SCAGB205",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000145078",
		"Description": "CAMDEN SAMPLE SALE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "INACTIVE Bridehill LTD T-A Itso Me",
	"Number": "CGB000288",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007816",
		"Description": "Bridehill LTD T-A Itso Me",
		"email": "scronin@shipton.ie; abree@shipton.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "jack napier",
	"Number": "CGB001563",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwicks - Brent Cross",
	"Number": "CUKCC0046",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149568",
		"Description": "Fenwicks - Brent Cross",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Parkstone LTD T-A Hoopers [Hgate]",
	"Number": "CGB000130",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007728",
		"Description": "Parkstone LTD T-A Hoopers [Hgate]",
		"email": "tonyarmstrong@hoppers.ltd.uk mensadmin@hoopersstores.com katie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Accessories Finland Oy",
	"Number": "CGB000177",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007389",
		"Description": "Accessories Finland Oy",
		"email": "marko@acc3ss.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sumla Mughal",
	"Number": "CGB002713",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jordan Dick",
	"Number": "CGB004305",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000032033",
		"Description": "Jordan Dick",
		"email": "jordan_dick@aol.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sophie Davidson",
	"Number": "CGB002008",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Leecajay LTD T-A Louis Copeland",
	"Number": "CGB000857",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007525",
		"Description": "Leecajay LTD T-A Louis Copeland",
		"email": "louisj@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "David Thomson And Son",
	"Number": "CGB000099",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007266",
		"Description": "David Thompson And Son",
		"email": "dtjed@aol.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "N-O-W Clare LTD T-A Nigel Clare",
	"Number": "CGB001682",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024724",
		"Description": "N-O-W Clare LTD T-A Nigel Clare",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Chris Taylor",
	"Number": "CGB001749",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Kori Richards",
	"Number": "CGB004782",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Svetlana Matkovska",
	"Number": "CGB001794",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "The English Sports Shop  T-A Cecile",
	"Number": "CGB000329",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007340",
		"Description": "English Sports Shop LTD - Forwarder",
		"email": "keithh@fsmac.com paulj@fsmac.com lloyd@fsmac.com -director",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "poppy stalker",
	"Number": "CGB004051",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Chih-Ming Huang",
	"Number": "CGB002403",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "chris muir",
	"Number": "CGB004083",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031522",
		"Description": "Chris Muir Misc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Neil Mellor",
	"Number": "CGB003374",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jeremiah Tedeku",
	"Number": "CGB001734",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Camp Hopson [Newbury] LTD",
	"Number": "CGB001275",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007117",
		"Description": "Camp Hopson [Newbury] LTD",
		"email": "anthony.ohagan@camphopson.co.uk; kiki.kettunen@camphopson.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Georgia Ellis",
	"Number": "CGB001588",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "EURL Capsarah t/a Loft",
	"Number": "CGB001360",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000023987",
		"Description": "EURL Capsarah t/a Loft",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hilton Bournemouth",
	"Number": "CGB006598",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037251",
		"Description": "Hilton Bournemouth",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird  [Jersey]  LTD",
	"Number": "CGB000741",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006920",
		"Description": "Ashworth And Bird  [Jersey]  LTD",
		"email": "linda@ashworthandbird.com",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000038497",
		"Description": "Ashworth & Bird [Jersey] Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Selfridges - Online",
	"Number": "CUKCC0005",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149527",
		"Description": "Selfridges - Online",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALEXANDRA KNOCH",
	"Number": "CGB002987",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "M And M Direct LTD Samples",
	"Number": "CGB000162",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008010",
		"Description": "M And M Direct LTD Sample Deliv",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Imperia",
	"Number": "CGB010679",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000142921",
		"Description": "Imperia",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "BILLY CHONG",
	"Number": "CGB004697",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB329 Customer",
	"Number": "SCAGB329",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005633",
		"Description": "HOUSE OF FRASER WESTFIELD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarl Syam T-A L Appart",
	"Number": "CGB000937",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006607",
		"Description": "Sarl Syam T-A L Appart",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Noura Mohammed",
	"Number": "CGB003140",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Rob Murgatroyd",
	"Number": "CGB009783",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nichole White",
	"Number": "CGB004084",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031523",
		"Description": "Nichole White Misc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mccartan Bros",
	"Number": "CGB001052",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007938",
		"Description": "Mccartan Bros",
		"email": "mccartanbros@tiscali.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Johanna Fleming",
	"Number": "CGB005599",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035028",
		"Description": "do not use",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wp Brown LTD T-A Browns York",
	"Number": "CGB000846",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008465",
		"Description": "Wp Brown LTD T-A Browns York",
		"email": "mcd@browns-york.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ALENA POSPISILOVA",
	"Number": "CGB002187",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB607 Customer",
	"Number": "SCAGB607",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000146665",
		"Description": "TED BIG DC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gatwick North",
	"Number": "CGB001574",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018924",
		"Description": "Gatwick North",
		"email": "246.gatwicknorth@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apriori",
	"Number": "CGB001041",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006621",
		"Description": "Apriori",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "A de Gruchy & Co Ltd",
	"Number": "CGB007086",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000038642",
		"Description": "A de Gruchy & Co Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jonny Edwards",
	"Number": "CGB001919",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002533",
		"Description": "Jonny Edwards",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Hull (Closed)",
	"Number": "CUKCC0039",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149561",
		"Description": "HOF - Hull (Closed)",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wdfg Uk LTD T-A World Duty Free",
	"Number": "CGB000008",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008411",
		"Description": "World Duty Free Europe LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB270 Stansted",
	"Number": "CGB003973",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031231",
		"Description": "GB270 Stansted",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Berwin Retail LTD",
	"Number": "CGB000021",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006733",
		"Description": "Berwin Retail LTD",
		"email": "l.wrightson@berwinberwin.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Janine Ellershaw",
	"Number": "CGB002103",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jason Rodger",
	"Number": "CGB006031",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Roomes Dept Store - Inv And Deliv",
	"Number": "CGB000180",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007298",
		"Description": "Roomes Dept Store - Inv And Deliv",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laura Matthews",
	"Number": "CGB001341",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Titanbrook LTD T-A Maysons",
	"Number": "CGB000100",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008597",
		"Description": "Titanbrook LTD T-A Maysons",
		"email": "sean@maysons.net",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Goods",
	"Number": "CGB000028",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007680",
		"Description": "Goods",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "BDJShop SL",
	"Number": "CGB005833",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034050",
		"Description": "BDJShop SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Andrea Alistar",
	"Number": "CGB002735",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Anna Kubiak",
	"Number": "CGB001755",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Callum Macfarlane",
	"Number": "CGB002951",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Emma Gibson",
	"Number": "CGB002024",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird [Jersey] LTD",
	"Number": "CGB000868",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008273",
		"Description": "Ashworth And Bird [Jersey] LTD",
		"email": "linda@collectionsgroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lauren Evans",
	"Number": "CGB005917",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Siempre Almere",
	"Number": "CGB000820",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007888",
		"Description": "Siempre Almere",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SELFRIDGES - EDI Acc",
	"Number": "CUKSE1000",
	"Group": "R-CON-SEL",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149891",
		"Description": "SELFRIDGES - EDI Acc",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Sloane Square",
	"Number": "CUKCC0057",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149579",
		"Description": "John Lewis - Sloane Square",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Isabella Di Giuseppe",
	"Number": "CGB007851",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Matt Fisher",
	"Number": "CGB003548",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Anna Van Toor",
	"Number": "CGB001744",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024183",
		"Description": "Anna Van Toor",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Central",
	"Number": "CUKCC0091",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149613",
		"Description": "John Lewis - Central",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Max Ephson",
	"Number": "CGB001340",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Amy Morris",
	"Number": "CGB005906",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000035805",
		"Description": "Amy",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ben Mills",
	"Number": "CGB002586",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Queens",
	"Number": "CGB000835",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008254",
		"Description": "Queens",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ROBYN LISTER",
	"Number": "CGB001993",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031452",
		"Description": "misc.",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "W Mair T-A Acc Exit-Quest Clothing",
	"Number": "CGB000043",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007352",
		"Description": "W Mair T-A Acc Exit-Quest Clothing",
		"email": "sharon.walls@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Michela Soravia T-A Oyster Lingerie",
	"Number": "CGB000954",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008142",
		"Description": "Michela Soravia T-A Oyster Lingerie",
		"email": "m.soravia@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Julija Ivanova",
	"Number": "CGB001660",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Apple Uk - Syncreon",
	"Number": "CGB000694",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006597",
		"Description": "Apple Uk - Syncreon",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laura O'Hara",
	"Number": "CGB005547",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Deelux Trading AG - SMS",
	"Number": "CGB006481",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000037160",
		"Description": "Deelux Trading AG - SMS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "R And A Robinson",
	"Number": "CGB000187",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007238",
		"Description": "R And A Robinson",
		"email": "robert@inson.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird [Jersey] LTD",
	"Number": "CGB000869",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008287",
		"Description": "Ashworth And Bird [Jersey] LTD",
		"email": "linda@collectionsgroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Nottingham",
	"Number": "CUKCC0068",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149590",
		"Description": "John Lewis - Nottingham",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mission Menswear LTD",
	"Number": "CGB000077",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007976",
		"Description": "Mission Menswear LTD",
		"email": "steve_mission@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Shop Direct Home Shopping LTD",
	"Number": "CGB000017",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007593",
		"Description": "Glovers Transport LTD - [Lwds]",
		"email": "jo@gloverstransport.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "N-O-W Clare LTD T-A Maidens",
	"Number": "CGB000050",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008064",
		"Description": "N-O-W Clare LTD T-A Nigel Clare",
		"email": "nigel@nigelclare.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Reputation Menswear LTD",
	"Number": "CGB000277",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008329",
		"Description": "Reputation Menswear LTD",
		"email": "info@reputationmenswear.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB925 Customer",
	"Number": "SCAGB925",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005783",
		"Description": "JOHN LEWIS - BLUEWATER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB225 Customer",
	"Number": "SCAGB225",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005558",
		"Description": "BRAINTREE - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Allure",
	"Number": "CGB000780",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006862",
		"Description": "Allure",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "J E Beale PLC 04 Southport",
	"Number": "CGB000134",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006693",
		"Description": "J E Beale PLC 04 Southport",
		"email": "accounts@beales.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Country Attire LTD",
	"Number": "CGB001037",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007058",
		"Description": "Country Attire LTD",
		"email": "rebecca.coxon@countryattire.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Andrew Steven Colley DO NOT USE",
	"Number": "CGB003718",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005334",
		"Description": "207 York",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Folli Follie Group",
	"Number": "CGB000932",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006906",
		"Description": "Folli Follie Group - Goldair Cargo SA",
		"email": "stocco@follifollie.gr; nkarellis@follifollie.gr; zachari@follifollie.gr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bram ZNL der Konen Bekleidungshaus KG",
	"Number": "CGB001305",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018808",
		"Description": "Delivery",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Busybodys Lingerie Limited",
	"Number": "CGB004552",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000028383",
		"Description": "Busybodys Lingerie Limited",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Leicester",
	"Number": "CUKCC0073",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149595",
		"Description": "John Lewis - Leicester",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "MIRIAM BEARD",
	"Number": "CGB001417",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "College",
	"Number": "CGB001159",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007131",
		"Description": "College",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Hannah Spedding",
	"Number": "CGB003035",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lendi Bv",
	"Number": "CGB001141",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007557",
		"Description": "Lendi Bv",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Rivs",
	"Number": "CGB001056",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008321",
		"Description": "Rivs",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "SHANNON TALWAR",
	"Number": "CGB010429",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ros Mode",
	"Number": "CGB000836",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008341",
		"Description": "Ros Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Maria Rinaudo",
	"Number": "CGB002334",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarah Hague",
	"Number": "CGB009793",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bath Rugby - Fao Matt Powell",
	"Number": "CGB001152",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007012",
		"Description": "Bath Rugby - Fao Matt Powell",
		"email": "matt.powell@bathrugby.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg Retail Buying",
	"Number": "CGB001119",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008162",
		"Description": "Peek And  Cloppenburg Retail Buying",
		"email": "#log-import@peekundcloppenburg.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Danielle East",
	"Number": "CGB004498",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jake Harvey",
	"Number": "CGB008152",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis Samples",
	"Number": "CGB000123",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007417",
		"Description": "John Lewis",
		"email": "financialprocessing@partnershipservices.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Wild & Wolf LTD",
	"Number": "CGB001231",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013075",
		"Description": "WILD & WOLF LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And Cloppenburg Retail Buying",
	"Number": "CGB001045",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008156",
		"Description": "Peek And Cloppenburg Kg Store",
		"email": "#log-import@peekundcloppenburg.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ESTHER EZEGBE",
	"Number": "CGB009954",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Louis Copeland And Sons LTD",
	"Number": "CGB001112",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007541",
		"Description": "Louis Copeland And Sons LTD",
		"email": "david@louiscopeland.ie",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB206 Customer",
	"Number": "SCAGB206",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005541",
		"Description": "TED BAKER - LEEDS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lui Menswear Ltd t/a Per Lui",
	"Number": "CGB002623",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000012982",
		"Description": "Lui Menswear Ltd t/a Per Lui",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jake Badenoch",
	"Number": "CGB005815",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mr And Mrs Mc LTD T-A Neola Malahide",
	"Number": "CGB009153",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000044302",
		"Description": "Mr And Mrs Mc LTD T-A Neola Malahide",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB273 Customer",
	"Number": "SCAGB273",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000145085",
		"Description": "BICESTER POP UP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Elvis Chan",
	"Number": "CGB001750",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB233 Customer",
	"Number": "SCAGB233",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005563",
		"Description": "MANCHESTER SHAMBLES - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jake Griffin CPS",
	"Number": "CGB002606",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bentalls - Kingston",
	"Number": "CUKCC0043",
	"Group": "R-CON-FWB",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149565",
		"Description": "Bentalls - Kingston",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jh Masdings And Son LTD Ta Oxygen",
	"Number": "CGB000064",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008138",
		"Description": "Jh Masdings And Son LTD Ta Oxygen",
		"email": "danny@masdings.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kmi Brands",
	"Number": "CGB000196",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007485",
		"Description": "Kmi Brands",
		"email": "accounts@kmibrands.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Opaal Vught",
	"Number": "CGB000032",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008122",
		"Description": "Opaal Vught",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Boros Bromley LTD",
	"Number": "CGB000105",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006967",
		"Description": "Boros Bromley LTD",
		"email": "borosaccounts@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Manchester Trafford Centre",
	"Number": "CGB001570",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018940",
		"Description": "Manchester Trafford Centre",
		"email": "256.ManchesterTrafford@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Andrew Somerville",
	"Number": "CGB009474",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB928 Customer",
	"Number": "SCAGB928",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005787",
		"Description": "JOHN LEWIS - CARDIFF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Meila Jamadar",
	"Number": "CGB001658",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Nafisa Raheem",
	"Number": "CGB008834",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000142125",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sarl Jema T-A Donna",
	"Number": "CGB000842",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007250",
		"Description": "Sarl Jema T-A Donna",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Lakeside",
	"Number": "CUKCC0035",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149557",
		"Description": "HOF - Lakeside",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - High Wycombe",
	"Number": "CUKCC0080",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149602",
		"Description": "John Lewis - High Wycombe",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Barnabas Medve",
	"Number": "CGB001633",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Domino Style LTD T-A Domino",
	"Number": "CGB000071",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007954",
		"Description": "Domino Style LTD T-A Domino",
		"email": "janestillwell@live.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Nigel Holmes Ltd",
	"Number": "CGB001805",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000012998",
		"Description": "Nigel Holmes Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird [Jersey] LTD",
	"Number": "CGB000870",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008285",
		"Description": "Ashworth And Bird [Jersey] LTD",
		"email": "linda@ashworthandbird.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Trendy Orihuela 2015 SL t/a BTQ Trendy",
	"Number": "CGB003296",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026531",
		"Description": "Trendy Orihuela 2015 SL t/a BTQ Trendy",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB322 Customer",
	"Number": "SCAGB322",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005626",
		"Description": "HOUSE OF FRASER DARLINGTON",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Samantha Young",
	"Number": "CGB001615",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Active In Style Ltd",
	"Number": "CGB006425",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000036717",
		"Description": "Active In Style Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lora Stamenova",
	"Number": "CGB001568",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Josiahs LTD",
	"Number": "CGB000065",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007443",
		"Description": "Josiahs LTD",
		"email": "josiahs@btconnect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bentalls PLC Supplier Accounts",
	"Number": "CGB000011",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006727",
		"Description": "Bentalls PLC Supplier Accounts",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Izabela Chasikowska",
	"Number": "CGB001622",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Apple Hong Kong",
	"Number": "CGB000686",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006611",
		"Description": "Apple Hong Kong",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Francesca Quilley-Smith",
	"Number": "CGB010198",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "British Ceramic Tile Ltd",
	"Number": "CGB005182",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034027",
		"Description": "British Ceramic Tiles Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Majona 3 SC t/a Coco",
	"Number": "CGB005837",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034408",
		"Description": "Majona 3 SC t/a Coco",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Darren Sutliff",
	"Number": "CGB005617",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Roons",
	"Number": "CGB000943",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008337",
		"Description": "Roons",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Justyna Lisowska",
	"Number": "CGB009712",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Chantilly [Stone] LTD T-A Chantilly",
	"Number": "CGB000995",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007159",
		"Description": "Chantilly [Stone] LTD T-A Chantilly",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Arwinder Chahal",
	"Number": "CGB001636",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB314 Customer",
	"Number": "SCAGB314",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005612",
		"Description": "HOUSE OF FRASER GATESHEAD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Alpha Lsg LTD - Stansted",
	"Number": "CGB000605",
	"Group": "W-DUTYFREE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006844",
		"Description": "Alpha Flight Uk LTD - Tcx Stn",
		"email": "ltanner@alpha-group.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Driss Fashion",
	"Number": "CGB000824",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007262",
		"Description": "Driss Fashion",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kalma Trends S.L",
	"Number": "CGB003211",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000026529",
		"Description": "Kalma Trends S.L",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Harold Crabtree - Statements",
	"Number": "CGB000650",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007137",
		"Description": "Harold Crabtree - Statements",
		"email": "info@haroldcrabtree.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "KELLY FORD",
	"Number": "CGB007754",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "WP Brown Ltd t/a Browns Beverley",
	"Number": "CGB008594",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000043708",
		"Description": "WP Brown Ltd t/a Browns of York",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Schustermann & Borenstein GmbH",
	"Number": "CGB008872",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045120",
		"Description": "Schustermann & Borenstein GmbH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Cardiff",
	"Number": "CUKCC0014",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149536",
		"Description": "HOF - Cardiff",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Gavin Richards - Misc",
	"Number": "CGB005232",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lapel Stafford LTD T-A Lapel",
	"Number": "CGB000112",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007607",
		"Description": "Lapel Stafford LTD T-A Lapel",
		"email": "richard@lapelclothing.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jujhar Singh",
	"Number": "CGB009443",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Rosie Brennan",
	"Number": "CGB001357",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Beau",
	"Number": "CGB000912",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006713",
		"Description": "Beau",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Leurink Mode",
	"Number": "CGB000831",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007565",
		"Description": "Leurink Mode",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Pelham Leather Goods LTD",
	"Number": "CGB001064",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008185",
		"Description": "Pelham Leather Goods LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mondottica LTD  Royalty",
	"Number": "CGB001192",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013011",
		"Description": "MONDOTTICA LTD  ROYALTY",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Eva Divine SL",
	"Number": "CGB007446",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041176",
		"Description": "Eva Divine SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kredo",
	"Number": "CGB001167",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007493",
		"Description": "Kredo",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Laurie Lyons",
	"Number": "CGB010326",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Ws Pettit And Co LTD [Bev]",
	"Number": "CGB000095",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008232",
		"Description": "Ws Pettit And Co LTD [Bev]",
		"email": "paul@pettits.com; donna@pettits.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Hut.Com LTD",
	"Number": "CGB000739",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007795",
		"Description": "The Hut.Com LTD",
		"email": "ukinvoices@thehutgroup.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ted Baker Downunder Pty - Ex Uk",
	"Number": "CGB000150",
	"Group": "W-JV",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008547",
		"Description": "Ted Baker Downunder Pty - Ex Uk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Bernard",
	"Number": "CGB001316",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000018830",
		"Description": "Bernard",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jennie Sykes",
	"Number": "CGB001579",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB311 Customer",
	"Number": "SCAGB311",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005608",
		"Description": "HOUSE OF FRASER SHEFFIELD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "FOLLI-FOLLIE GROUP SMS GBP",
	"Number": "CGB003438",
	"Group": "W-SMS",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030014",
		"Description": "FOLLI-FOLLIE GROUP SMS GBP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "The Chapar Ltd",
	"Number": "CGB003031",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000025298",
		"Description": "The Chapar Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB244 Customer",
	"Number": "SCAGB244",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005572",
		"Description": "BATH - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "CHLOE O CONNOR MISC",
	"Number": "CGB009821",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000062348",
		"Description": "CHLOE O CONNOR MISC",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Galleries [Fashions] LTD T-A Circus",
	"Number": "CGB000127",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008583",
		"Description": "Galleries [Fashions] LTD T-A Circus",
		"email": "rachel.richards@thackerays.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB917 Customer",
	"Number": "SCAGB917",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005774",
		"Description": "JOHN LEWIS - NEWCASTLE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Victoria Fawcett",
	"Number": "CGB001379",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Grosvenor [Aberdeen] LTD",
	"Number": "CGB000084",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007892",
		"Description": "Grosvenor [Aberdeen] LTD",
		"email": "patrick.ritchie@signature-menswear.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Non Ted HOF Concession",
	"Number": "CUKCC0054",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149576",
		"Description": "Non Ted HOF Concession",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "W D Coe LTD",
	"Number": "CGB000039",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008407",
		"Description": "W D Coe LTD",
		"email": "info@coes.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "josie eatock",
	"Number": "CGB003893",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Andy lippiello",
	"Number": "CGB001413",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Lauren Bell",
	"Number": "CGB004869",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Anna Simpson",
	"Number": "CGB002922",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Competitive Edge [Ormskirk] LTD",
	"Number": "CGB000147",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007069",
		"Description": "Competitive Edge [Ormskirk] LTD",
		"email": "rachelleatherton@yahoo.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Portmeirion Group UK LTD",
	"Number": "CGB001813",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000024884",
		"Description": "Portmeirion Group UK LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dani Marie Hatcher",
	"Number": "CGB004864",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Galleries [Fashions] LTD",
	"Number": "CGB000701",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008593",
		"Description": "Galleries [Fashions] LTD",
		"email": "rr@gflgroup.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Virgin Racing Limited",
	"Number": "CGB001145",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008379",
		"Description": "Virgin Racing Limited",
		"email": "lucy.rillstone@virginracing.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jessica Pradhan",
	"Number": "CGB001575",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sean Harrison",
	"Number": "CGB008267",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Mr B Green t/a Hewetts",
	"Number": "CGB005843",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000034868",
		"Description": "Mr B Green t/a Hewetts",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ella-Jennifer Lamprell",
	"Number": "CGB008878",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Christi T-A Christi Boutique",
	"Number": "CGB000764",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007093",
		"Description": "Christi T-A Christi Boutique",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Holly Murray",
	"Number": "CGB010397",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Sage Clothing LTD",
	"Number": "CGB000242",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007880",
		"Description": "Sage Clothing LTD",
		"email": "jyoti.mamnani@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Albion 1879 SL",
	"Number": "CGB003312",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000027729",
		"Description": "Albion 1879 SL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "L Collections Ltd",
	"Number": "CGB009608",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000061693",
		"Description": "L Collections Ltd",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Guilt Lingerie Ltd",
	"Number": "CGB000295",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007670",
		"Description": "Guilt Lingerie Llp - Invoices",
		"email": "admin@guiltlingerie.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LM Magdalena",
	"Number": "CGB001789",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Van Der Kam Gouda",
	"Number": "CGB001156",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008675",
		"Description": "Van Der Kam Mens And Womens",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ramneet Lall - FAF",
	"Number": "CGB003368",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030617",
		"Description": "Ramneet Lall FAF",
		"email": "rameet_1995@hotmail.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Joseph McMillan",
	"Number": "CGB009068",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "lauren white",
	"Number": "CGB010117",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "El Corte Ingles",
	"Number": "CGB007407",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000040946",
		"Description": "ECI Valdemoro Warehouse",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000045576",
		"Description": "ECI Sevilla Warehouse",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000045577",
		"Description": "ECI Barcelona Warehouse",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Corey Wickenden Wickenden",
	"Number": "CGB009295",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Bssa The Hanger - Showroom",
	"Number": "CGB000892",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008587",
		"Description": "Bssa The Hanger - Showroom",
		"email": "lhollander@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sam Wright",
	"Number": "CGB003892",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Bluewater",
	"Number": "CUKCC0079",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149601",
		"Description": "John Lewis - Bluewater",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Outletcity Metzingen GmbH",
	"Number": "CGB007507",
	"Group": "W-OFFPRICE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000041344",
		"Description": "Outletcity @ PVS Schenk",
		"email": "",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000042696",
		"Description": "Outletcity Metzingen Gmbh SMS",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB901 Customer",
	"Number": "SCAGB901",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005753",
		"Description": "JOHN LEWIS - CHEADLE",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Noosa Khogali",
	"Number": "CGB003323",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB332 Customer",
	"Number": "SCAGB332",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005636",
		"Description": "HOUSE OF FRASER GLASGOW",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Richie Fenwick FAF",
	"Number": "CGB009597",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149675",
		"Description": "Richie Fenwick",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Martyna Kapuscinska",
	"Number": "CGB001365",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Desiderio 2000 SL t/a BTQ Yeyo",
	"Number": "CGB003315",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000028381",
		"Description": "Desiderio 2000 SL t/a BTQ Yeyo",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Kyle Arkless",
	"Number": "CGB005628",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "laura Pike",
	"Number": "CGB008587",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000045601",
		"Description": "laura pike",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Online",
	"Number": "CUKCC0006",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149528",
		"Description": "HOF - Online",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sheila Mulligan T-A The Hanger",
	"Number": "CGB000589",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007736",
		"Description": "Sheila Mulligan T-A The Hanger",
		"email": "thehangerboutique@gmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwicks Ltd [Brent X]",
	"Number": "CGB000136",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007366",
		"Description": "Fenwicks [Brent X]",
		"email": "brx.supplieraccounts12@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	},
	{
		"LocationId": "000007374",
		"Description": "Fenwicks LTD",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek And  Cloppenburg",
	"Number": "CGB001180",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006585",
		"Description": "Peek And  Cloppenburg Ax Inv Code",
		"email": "#log-import@ansons.de;matthias.guese@ansons.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Stratford",
	"Number": "CUKCC0084",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149606",
		"Description": "John Lewis - Stratford",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "ZAIN MASHHOOD",
	"Number": "CGB010605",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "GB907 Customer",
	"Number": "SCAGB907",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005785",
		"Description": "JOHN LEWIS - LIVERPOOL",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fenwick LTD [Windsor]",
	"Number": "CGB000140",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006929",
		"Description": "Fenwick LTD [Windsor]",
		"email": "windsoraccounts@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "francesca quilley",
	"Number": "CGB005693",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Budwal Bakhtawar t/a  Budwals",
	"Number": "CGB000082",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007032",
		"Description": "Budwals",
		"email": "sales@budwals.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ellie Bevis",
	"Number": "CGB003207",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Fenwicks ltd",
	"Number": "CGB001178",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007376",
		"Description": "Fenwicks Ax Inv Code",
		"email": "supplieraccs@fenwick.co.uk",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Albion 1879 - 338 Arenal",
	"Number": "CGB000188",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006763",
		"Description": "Albion 1879 - 338 Arenal",
		"email": "nigel@albionspain.com; mayra@albionspain.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB325 Customer",
	"Number": "SCAGB325",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005631",
		"Description": "HOUSE OF FRASER EXETER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Demi Harper",
	"Number": "CGB001809",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Losanz Complements SL",
	"Number": "CGB003510",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029195",
		"Description": "Losanz Complements S.L",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Oxford St",
	"Number": "CUKCC0017",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149539",
		"Description": "HOF - Oxford St",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Aristocrat Exclusive Ladies And",
	"Number": "CGB001034",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006625",
		"Description": "Aristocrat Exclusive Ladies And",
		"email": "lawrence@aristocrat.uk.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Southampton",
	"Number": "CUKCC0062",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149584",
		"Description": "John Lewis - Southampton",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "259 Glasgow",
	"Number": "CGB007823",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000002564",
		"Description": "259 Glasgow",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ess Dokkum",
	"Number": "CGB000959",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007334",
		"Description": "Ess Dokkum",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Albion 1879 - 338a Marbella",
	"Number": "CGB000562",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006767",
		"Description": "Albion 1879 - 338a Marbella",
		"email": "nigel@albionspain.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Lamoda GmbH",
	"Number": "CGB006907",
	"Group": "W-HOMESHOP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000029143",
		"Description": "Lamoda GmbH",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amy Davies",
	"Number": "CGB009134",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Soho",
	"Number": "CGB001138",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008505",
		"Description": "Soho",
		"email": "asya.k@hotmail.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB998 Customer",
	"Number": "SCAGB998",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000019241",
		"Description": "GB998 Customer",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Molly Smith FAF",
	"Number": "CGB003827",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000030897",
		"Description": "Molly Smith FAF",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Fluke Banstead LTD - Womens",
	"Number": "CGB000169",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007393",
		"Description": "Fluke Banstead LTD - Womens",
		"email": "kirsti@flukebanstead.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "LGM Developments Ltd t/a EJ Menswear",
	"Number": "CGB001802",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000012996",
		"Description": "EJ's Menswear",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jansen-Noy",
	"Number": "CGB000828",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007832",
		"Description": "Jansen-Noy",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jh Masdings And Son LTD Ta Masdings",
	"Number": "CGB000093",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007930",
		"Description": "Jh Masdings And Son LTD Ta Masdings",
		"email": "danny@masdings.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Mr Braithwaite",
	"Number": "CGB000987",
	"Group": "W-VIP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007016",
		"Description": "Mr Braithwaite",
		"email": "clare.ryan-kavanagh@tedbaker.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Matt Lidington",
	"Number": "CGB005571",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Jade McArdle",
	"Number": "CGB010191",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Phillip Blakeman",
	"Number": "CGB001900",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000033128",
		"Description": "FAF 40%",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ashworth And Bird [Reigate]  LTD",
	"Number": "CGB000861",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008265",
		"Description": "Ashworth And Bird [Jersey]  LTD",
		"email": "reigate@ashworthandbird.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Sarl T.Ch. T -A Two Cour",
	"Number": "CGB001059",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008635",
		"Description": "Sarl T.Ch. T -A Two Cour",
		"email": "mcarmoi@thehanger.fr",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Jozenzo",
	"Number": "CGB000829",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007439",
		"Description": "Jozenzo",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Charlotte Corfield McBride",
	"Number": "CGB003953",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000031189",
		"Description": "Charlotte Corfield",
		"email": "Charanne7@gmil.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "NORGENTA LATA",
	"Number": "CGB008378",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "HOF - Belfast",
	"Number": "CUKCC0032",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149554",
		"Description": "HOF - Belfast",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Ann Bryl T-A Bryls Boutique",
	"Number": "CGB000170",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007020",
		"Description": "Ann Bryl T-A Bryls Boutique",
		"email": "annbryl5@hotmail.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "John Lewis - Liverpool",
	"Number": "CUKCC0061",
	"Group": "R-CON-JLP",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149583",
		"Description": "John Lewis - Liverpool",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Apple Pty LTD C-O Schenker Aust",
	"Number": "CGB000689",
	"Group": "W-B2B",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006601",
		"Description": "Apple Pty LTD C-O Schenker Aust",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Peek & Cloppenburg Retail Buying Gmbh & Co KG",
	"Number": "CGB001105",
	"Group": "W-MULTIPLE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000006882",
		"Description": "Ansons - Langenfeld Dc",
		"email": "#log-import@ansons.de;matthias.guese@ansons.de",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - White City",
	"Number": "CUKCC0025",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149547",
		"Description": "HOF - White City",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Amy Green",
	"Number": "CGB003301",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Eduard Gavrilenko",
	"Number": "CGB001564",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Yun San Corp",
	"Number": "CGB001221",
	"Group": "R-LICENSEE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000013061",
		"Description": "YUN SAN CORP",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Dounia Abbaze",
	"Number": "CGB001772",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "",
		"Description": "",
		"email": "",
		"shipConsolidated": false,
		"success": false,
		"errorMessage": "(Validation Error: Location ID[] is mandatory)(Validation Error: Description[] is mandatory)",
		"errorNumber": "ESB001"
	}],
	"success": true
},
{
	"AccountName": "Raak",
	"Number": "CGB001048",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000008261",
		"Description": "Raak",
		"email": "info@msbvr.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB240 Customer",
	"Number": "SCAGB240",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005568",
		"Description": "REGENT STREET - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "GB228 Customer",
	"Number": "SCAGB228",
	"Group": "R-RETAILCU",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000005560",
		"Description": "BELFAST - TED BAKER",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "HOF - Plymouth",
	"Number": "CUKCC0028",
	"Group": "R-CON-HOF",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000149550",
		"Description": "HOF - Plymouth",
		"email": "",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
},
{
	"AccountName": "Cruise Clothing LTD",
	"Number": "CGB000600",
	"Group": "W-INDIE",
	"Currency": "GBP",
	"Contacts": [{
		"LocationId": "000007063",
		"Description": "Cruise Clothing LTD",
		"email": "jill.coverdale@sportsdirect.com",
		"shipConsolidated": false,
		"success": true
	}],
	"success": true
}]